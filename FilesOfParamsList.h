/*============================================================================*/
/*                          Files of Parameters Class                         */
/*============================================================================*/

/*!
 * \file FilesOfParamsList.h
 * \brief Parameters files list managing class
 * \author Damien Georges
 * \version 1.0
 */
 
#ifndef FOPL_H
#define FOPL_H

#include "FGUtils.h"
#include <boost/filesystem.hpp>

using namespace std;


/*!
 * \class FOPL
 * \brief Parameters files list managing class
 *
 * This class contains a set of parameters corresponding to parameters text
 * files. It allows correct reading and creation. All paths should be given as
 * relative paths.
 * Basic parameters are mandatory : they concern global simulation (Global 
 * simulations parameters, saving state and directory, ...), timings (to
 * save abundances and objects), spatial characteristics (mask) and functional
 * groups (life history, ...).
 * If some modules are activated (dispersal, habitat suitability, light, ...),
 * some specific parameters might also be required.
 */

class FOPL
{
	private:
	
	/* Global simulation parameters */
	string m_GlobSimulParams; /*!< path to global simul params file */
	
	/* Saving parameters */
	string m_SavedState; /*!< path to previous FATEHDD simulation output objects */
	string m_SavingDir; /*!< Saving directory path */
	string m_SummArrSavingTimes; /*!< path to file containing summarised arrays saving dates */
	string m_ObjectsSavingTimes; /*!< path to file containing simul state objects saving dates */
	
	/* Spatial parameters */
	string m_Mask; /*!< path to .asc mask file */
	vector<string> m_HabitatMask; /*!< list of path to habitats masks */
	vector<string> m_DisturbMask; /*!< list of path to disturbances masks */
	vector<string> m_FireMask; /*!< list of path to fire disturbances masks */
	vector<string> m_ClimDataMask; /*!< list of path to climatic data masks */
	string m_MoistIndexMask; /*!< path to moisture index mask file */
	string m_SlopeMask; /*!< path to slope mask file */
	
	// TODO (damien#1#): change way disturbances params are given as smth like :
	/*
	time=0
	dist1=..
	dist2=...
	dist3=...
	
	time=xx
	dist1=...
	dist2=...
	dist3=...
	*/
	
	/* Simulation Timing parameters */
	vector<string> m_MaskChangeMasks; /*!< list of files containing list of masks of studied area change scenario */
	string m_MaskChangeTimes; /*!< list of studied area changes times */
	vector<string> m_HabChangeMasks; /*!< list of files containing list of masks of habitat change scenario */
	string m_HabChangeTimes; /*!< list of habitat changes times */
	vector<string> m_DistChangeMasks; /*!< list of files containing list of masks of land use change scenario */
	string m_DistChangeTimes; /*!< list of land use changes times */
	vector<string> m_FireChangeMasks; /*!< list of files containing list of masks of fire change scenario */
	string m_FireChangeTimes; /*!< list of land use changes times */
	vector<string> m_FireChangeFrequencies; /*!< list of files containing list of fire frequencies files change scenario */
	string m_FireFreqChangeTimes; /*!< list of fire frequencies changes times */
	vector<string> m_ClimDataChangeMasks; /*!< list of files containing list of masks of climatic data (precipitation, PET) */
	string m_ClimDataChangeTimes; /*!< list of climatic data changes times */
	vector<string> m_MoistIndexChangeMasks; /*!< list of files containing list of masks of moisture index change scenario */
	string m_MoistIndexChangeTimes; /*!< list of moisture index changes times */
	vector<string> m_AliensChangeMasks;	/*!< list of files containing list of masks of aliens introduction change scenario */
	string m_AliensChangeTimes; /*!< list of aliens introduction changes times */
	vector<string> m_AliensChangeFrequencies;	/*!< list of files containing list of aliens introduction frequencies files change scenario */
	string m_AliensFreqChangeTimes; /*!< list of aliens introduction frequencies changes times */
	
	/* FG specific parameters */
	vector<string> m_FGLifeHistory; /*!< list of path to FG life history parameters files */
	vector<string> m_FGLight; /*!< list of path to FG light parameters files */
	vector<string> m_FGHabSuitMaps; /*!< list of path to FG habitat suitability maps */
	vector<string> m_FGDispersal; /*!< list of path to FG dispersal parameters files */
	vector<string> m_FGDisturbance; /*!< list of path to FG disturbance parameters files */
	vector<string> m_FGSoil; /*!< list of path to FG soil parameters files */
	vector<string> m_FGFire; /*!< list of path to FG fire disturbance parameters files */
	vector<string> m_FGDrought; /*!< list of path to FG drought disturbance parameters files */
	vector<string> m_FGCondInitMaps; /*!< list of path to FG introduction points maps */
	
	/* Habitat baseline statistics */
	vector<string> m_HabitatBLstats; /*!< list of path to habitat baseline statistics files */
	
	/*-------------------------------------------*/
	/* Serialization function -------------------*/
	/*-------------------------------------------*/
	
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int /*version*/)
	{
		//cout << "> Serializing Files of Parameters..." << endl;
		ar & m_GlobSimulParams;
		ar & m_SavedState;
		ar & m_SavingDir;
		ar & m_SummArrSavingTimes;
		ar & m_ObjectsSavingTimes;
		ar & m_Mask;
		ar & m_HabitatMask;
		ar & m_DisturbMask;
		ar & m_FireMask;
		ar & m_MoistIndexMask;
		ar & m_SlopeMask;
		ar & m_ClimDataMask;
		ar & m_MaskChangeMasks;
		ar & m_MaskChangeTimes;
		ar & m_HabChangeMasks;
		ar & m_HabChangeTimes;
		ar & m_DistChangeMasks;
		ar & m_DistChangeTimes;
		ar & m_FireChangeMasks;
		ar & m_FireChangeTimes;
		ar & m_FireChangeFrequencies;
		ar & m_FireFreqChangeTimes;
		ar & m_ClimDataChangeMasks;
		ar & m_ClimDataChangeTimes;
		ar & m_MoistIndexChangeMasks;
		ar & m_MoistIndexChangeTimes;
		ar & m_FGLifeHistory;
		ar & m_FGLight;
		ar & m_FGHabSuitMaps;
		ar & m_FGDispersal;
		ar & m_FGDisturbance;
		ar & m_FGSoil;
		ar & m_FGFire;
		ar & m_FGDrought;
		ar & m_FGCondInitMaps;
		ar & m_HabitatBLstats;
		ar & m_AliensChangeMasks;
		ar & m_AliensChangeTimes;
		ar & m_AliensChangeFrequencies;
		ar & m_AliensFreqChangeTimes;
	}
	
	public:
	
	/*-------------------------------------------*/
	/* Constructors -----------------------------*/
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Default constructor
	 *
	 *	FOPL default constructor => All parameters are set to 0, False or None
	 */
	FOPL();
	
	/*!
	 *	\brief Full constructor
	 *
	 *	FOPL full constructor
	 *
	 *	\param paramSimulFile : path to text file containing well-formatted
	 * simulation related parameters
	 */
	FOPL(string paramSimulFile);
	
	/*-------------------------------------------*/
	/* Destructor -------------------------------*/
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Destructor
	 *
	 *	FOPL destructor
	 */
	~FOPL();
	
	/*-------------------------------------------*/
	/* Operators --------------------------------*/
	/*-------------------------------------------*/
	
	bool operator==(const FOPL& o) const
	{
		return (m_GlobSimulParams == o.m_GlobSimulParams &&
		m_SavedState == o.m_SavedState &&
		m_SavingDir == o.m_SavingDir &&
		m_SummArrSavingTimes == o.m_SummArrSavingTimes &&
		m_ObjectsSavingTimes == o.m_ObjectsSavingTimes &&
		m_Mask == o.m_Mask &&
		m_HabitatMask == o.m_HabitatMask &&
		m_DisturbMask == o.m_DisturbMask &&
		m_FireMask == o.m_FireMask &&
		m_ClimDataMask == o.m_ClimDataMask &&
		m_MoistIndexMask == o.m_MoistIndexMask &&
		m_SlopeMask == o.m_SlopeMask &&
		m_MaskChangeMasks == o.m_MaskChangeMasks &&
		m_MaskChangeTimes == o.m_MaskChangeTimes &&
		m_HabChangeMasks == o.m_HabChangeMasks &&
		m_HabChangeTimes == o.m_HabChangeTimes &&
		m_DistChangeMasks == o.m_DistChangeMasks &&
		m_DistChangeTimes == o.m_DistChangeTimes &&
		m_FireChangeMasks == o.m_FireChangeMasks &&
		m_FireChangeTimes == o.m_FireChangeTimes &&
		m_FireChangeFrequencies == o.m_FireChangeFrequencies &&
		m_FireFreqChangeTimes == o.m_FireFreqChangeTimes &&
		m_ClimDataChangeMasks == o.m_ClimDataChangeMasks &&
		m_ClimDataChangeTimes == o.m_ClimDataChangeTimes &&
		m_MoistIndexChangeMasks == o.m_MoistIndexChangeMasks &&
		m_MoistIndexChangeTimes == o.m_MoistIndexChangeTimes &&
		m_FGLifeHistory == o.m_FGLifeHistory &&
		m_FGLight == o.m_FGLight &&
		m_FGHabSuitMaps == o.m_FGHabSuitMaps &&
		m_FGDispersal == o.m_FGDispersal &&
		m_FGDisturbance == o.m_FGDisturbance &&
		m_FGSoil == o.m_FGSoil &&
		m_FGFire == o.m_FGFire &&
		m_FGDrought == o.m_FGDrought &&
		m_FGCondInitMaps == o.m_FGCondInitMaps &&
		m_HabitatBLstats == o.m_HabitatBLstats &&
		m_AliensChangeMasks == o.m_AliensChangeMasks &&
		m_AliensChangeTimes == o.m_AliensChangeTimes &&
		m_AliensChangeFrequencies == o.m_AliensChangeFrequencies &&
		m_AliensFreqChangeTimes == o.m_AliensFreqChangeTimes);
	}
	
	/*-------------------------------------------*/
	/* Getters & Setters ------------------------*/
	/*-------------------------------------------*/
	
	const string& getGlobSimulParams() const;
	const string& getSavedState() const;
	const string& getSavingDir() const;
	const string& getSummArrSavingTimes() const;
	const string& getObjectsSavingTimes() const;
	const string& getMask() const;
	const vector<string>& getHabitatMask() const;
	const vector<string>& getDisturbMask() const;
	const vector<string>& getFireMask() const;
	const vector<string>& getClimDataMask() const;
	const string& getMoistIndexMask() const;
	const string& getSlopeMask() const;
	const vector<string>& getMaskChangeMasks() const;
	const string& getMaskChangeTimes() const;
	const vector<string>& getHabChangeMasks() const;
	const string& getHabChangeTimes() const;
	const vector<string>& getDistChangeMasks() const;
	const string& getDistChangeTimes() const;
	const vector<string>& getFireChangeMasks() const;
	const string& getFireChangeTimes() const;
	const vector<string>& getFireChangeFrequencies() const;
	const string& getFireFreqChangeTimes() const;
	const vector<string>& getClimDataChangeMasks() const;
	const string& getClimDataChangeTimes() const;
	const vector<string>& getMoistIndexChangeMasks() const;
	const string& getMoistIndexChangeTimes() const;
	const vector<string>& getFGLifeHistory() const;
	const vector<string>& getFGLight() const;
	const vector<string>& getFGHabSuitMaps() const;
	const vector<string>& getFGDispersal() const;
	const vector<string>& getFGDisturbance() const;
	const vector<string>& getFGSoil() const;
	const vector<string>& getFGFire() const;
	const vector<string>& getFGDrought() const;
	const vector<string>& getFGCondInitMaps() const;
	const vector<string>& getHabitatBLstats() const;
	const vector<string>& getAliensChangeMasks() const;
	const string& getAliensChangeTimes() const;
	const vector<string>& getAliensChangeFrequencies() const;
	const string& getAliensFreqChangeTimes() const;
	
	void setGlobSimulParams(const string& globSimulParams);
	void setSavedState(const string& savedState);
	void setSavingDir(const string& savingDir);
	void setSummArrSavingTimes(const string& summArrSavingTimes);
	void setObjectsSavingTimes(const string& objectsSavingTimes);
	void setMask(const string& mask);
	void setHabitatMask(const vector<string>& habitatMask);
	void setDisturbMask(const vector<string>& disturbMask);
	void setFireMask(const vector<string>& fireMask);
	void setMoistIndexMask(const string& moistIndexMask);
	void setSlopeMask(const string& slopeMask);
	void setClimDataMask(const vector<string>& climDataMask);
	void setMaskChangeMasks(const vector<string>& maskChangeMasks);
	void setMaskChangeTimes(const string& maskChangeTimes);
	void setHabChangeMasks(const vector<string>& habChangeMasks);
	void setHabChangeTimes(const string& habChangeTimes);
	void setDistChangeMasks(const vector<string>& distChangeMasks);
	void setDistChangeTimes(const string& distChangeTimes);
	void setFireChangeMasks(const vector<string>& fireChangeMasks);
	void setFireChangeTimes(const string& fireChangeTimes);
	void setFireChangeFrequencies(const vector<string>& fireChangeFrequencies);
	void setFireFreqChangeTimes(const string& fireFreqChangeTimes);
	void setClimDataChangeMasks(const vector<string>& climDataChangeMasks);
	void setClimDataChangeTimes(const string& climDataChangeTimes);
	void setMoistIndexChangeMasks(const vector<string>& moistIndexChangeMasks);
	void setMoistIndexChangeTimes(const string& moistIndexChangeTimes);
	void setFGLifeHistory(const vector<string>& fgLifeHistory);
	void setFGLight(const vector<string>& fgLight);
	void setFGHabSuitMaps(const vector<string>& fgHabSuitMaps);
	void setFGDispersal(const vector<string>& fgDispersal);
	void setFGDisturbance(const vector<string>& fgDisturbance);
	void setFGSoil(const vector<string>& fgSoil);
	void setFGFire(const vector<string>& fgFire);
	void setFGDrought(const vector<string>& fgDrought);
	void setFGCondInitMaps(const vector<string>& fgCondInitMaps);
	void setHabitatBLstats(const vector<string>& habitatBLstats);
	void setAliensChangeMasks(const vector<string>& aliensChangeMasks);
	void setAliensChangeTimes(const string& aliensChangeTimes);
	void setAliensChangeFrequencies(const vector<string>& aliensChangeFrequencies);
	void setAliensFreqChangeTimes(const string& aliensFreqChangeTimes);
	
	/*-------------------------------------------*/
	/* Other functions --------------------------*/
	/*-------------------------------------------*/
	
	void show();

	/*!
	 *	\brief Check existence of parameter files retrieved : light competition
	 *
	 *	This function checks if all the files related to light competition module
	 * exist.
	 */
	void checkCorrectParams_light();
	
	/*!
	 *	\brief Check existence of parameter files retrieved : habitat suitability
	 *
	 *	This function checks if all the files related to habitat suitability
	 * module exist.
	 */
	void checkCorrectParams_habSuit();
	
	/*!
	 *	\brief Check existence of parameter files retrieved : dispersal
	 *
	 *	This function checks if all the files related to dispersal module exist.
	 */
	void checkCorrectParams_disp();
	
	/*!
	 *	\brief Check existence of parameter files retrieved : disturbances
	 *
	 *	This function checks if all the files related to disturbances module
	 * exist.
	 */
	void checkCorrectParams_dist();
	
	/*!
	 *	\brief Check existence of parameter files retrieved : soil competition
	 *
	 *	This function checks if all the files related to soil competition module
	 * exist.
	 */
	void checkCorrectParams_soil();
	
	/*!
	 *	\brief Check existence of parameter files retrieved : fire disturbances
	 *
	 *	This function checks if all the files related to fire disturbances module
	 * exist.
	 */
	void checkCorrectParams_fire();
	
	/*!
	 *	\brief Check existence of parameter files retrieved : drought disturbances
	 *
	 *	This function checks if all the files related to drought disturbances
	 * module exist.
	 */
	void checkCorrectParams_drought();
	
	/*!
	 *	\brief Check existence of parameter files retrieved : habitat stability
	 *
	 *	This function checks if all the files related to habitat stability check
	 * module exist.
	 */
	void checkCorrectParams_habStab();
	
	/*!
	 *	\brief Check existence of parameter files retrieved : aliens introduction
	 *
	 *	This function checks if all the files related to aliens introduction
	 * module exist.
	 */
	void checkCorrectParams_aliens();
	
	
	/*!
	 *	\brief Check existence of parameter files retrieved
	 *
	 *	This function checks if all the files given within the simulation file
	 * exist. It is the generic function, and parameter files can be tagged as
	 * optional, so required values are for basic parameters, such as the ones
	 * related to simulation parametrization (e.g. GLOBAL_PARAMS) or demographic
	 * model (e.g. PFG_LIFE_HISTORY_PARAMS). It also checks if the folder
	 * organization is correct and creates missing folders (especially for
	 * results).
	 */
	void checkCorrectParams();
	
	/*!
	 *	\brief Check existence of parameter files retrieved
	 *
	 *	This function checks if all the files given within the simulation file
	 * exist. It takes into account which modules are activated or not, and
	 * hence which files should be checked for existence.
	 * 
	 * \param doLight : is light competition module activated
	 * \param doHabSuit : is habitat suitability module activated
	 * \param doDisp : is dispersal module activated
	 * \param doDist : is disturbances module activated
	 * \param doSoil : is soil competition module activated
	 * \param doFire : is fire disturbances module activated
	 * \param doDrought : is drought disturbances module activated
	 * \param doHabStab : is habitat stability check module activated
	 * \param doAliens : is aliens introduction module activated
	 */
	void checkCorrectParams(const bool& doLight, const bool& doHabSuit, const bool& doDisp, const bool& doDist,
	const bool& doSoil, const bool& doFire, const bool& doDrought, const bool& doHabStab, const bool& doAliens);
	
	/*!
	 *	\brief Compare extension and coordinates of raster file with reference
	 *
	 *	This function checks if a given raster file has the same extension (tif
	 * or img) and the same coordinates (xmin, xmax, xres, xncell, ymin, ymax,
	 * yres, yncell) than a reference raster file (usually the one obtained from
	 * the tag --MASK-- within the simulation parameter file).
	 * 
	 * \param param : name of the concerned parameter
	 * \param file_name : path to raster file to be checked
	 * \param ext_REF : extension of the reference file
	 * \param coord_REF : coordinates of the reference file
	 */
	void testSameCoord(const string& param, const string& file_name, const string& ext_REF, Coordinates<double>& coord_REF);
	
	/*!
	 *	\brief Compare extension and coordinates of raster file with reference
	 *
	 *	This function checks if a list of raster files have the same extension
	 * (tif or img) and the same coordinates (xmin, xmax, xres, xncell, ymin,
	 * ymax, yres, yncell) than a reference raster file (usually the one
	 * obtained from the tag --MASK-- within the simulation parameter file).
	 * 
	 * \param param : name of the concerned parameter
	 * \param vector_name : a vector of paths to raster files to be checked
	 * \param ext_REF : extension of the reference file
	 * \param coord_REF : coordinates of the reference file
	 */
	void testSameCoord(const string& param, vector<string> vector_name, const string& ext_REF, Coordinates<double>& coord_REF);
	
	/*!
	 *	\brief Routine to compare extension and coordinates of raster file with
	 * reference
	 *
	 *	This function gets the information about the raster reference file (path,
	 * extension, coordinates) and compare them to all the raster files given
	 * within the simulation parameter file which will later be used in the
	 * simulation if the corresponding modules are activated (PFG_HAB_MASK,
	 * DIST_MASK, FIRE_MASK, CLIM_DATA_MASK, MOIST_MASK, ALIENS_MASK, HAB_MASK).
	 */
	void checkCorrectMasks();
};


BOOST_CLASS_VERSION(FOPL, 0)
#endif //FOPL_H

