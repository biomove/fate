/*============================================================================*/
/*                            Simulation Map Class                            */
/*============================================================================*/

/*!
 * \file SimulMap.h
 * \brief Spatial simulation map
 * \author Damien Georges
 * \version 1.0
 */
 
#ifndef SIMULMAP_H
#define SIMULMAP_H

#include "SuFateH.h"
#include "Disp.h"
#include "GlobalSimulParameters.h"
#include "FilesOfParamsList.h"
#include "FGUtils.h"

#include <iostream>
#include <string>
#include <sstream>
#include <boost/filesystem.hpp>
#include <omp.h>

typedef SuFate* SuFatePtr;
using namespace std;


/*!
 * \class SimulMap
 * \brief Spatial simulation map
 *
 * This class is the main brick of a FATE simulation.
 * It gathers all simulation parameters (GSP, FOPL), functional group
 * parameters (FG, FGresponse, ...) and spatial information (coordinates, mask,
 * ...). All spatial objects have the same coordinates and extent.
 * 
 * A mask of simulation map differentiates study/non-study pixels. To each
 * pixel is assigned a succession model. All succession models are linked to a
 * SpatialStack of seed maps, insuring communication between demographic and
 * dispersal model (if activated). A SpatialStack of habitat suitability maps
 * is also defined if the module is activated and linked as well to all
 * succession models.
 * In other words, each pixel has a succession model (SuFate), which can be
 * linked to habitat suitability maps (SuFateH). Disturbances, if activated,
 * are also supported at the pixel level. 
 */

class SimulMap
{
	private:
	
	GSP m_glob_params; /*!< Object containing simulation parameters */
	vector<FG> m_FGparams; /*!< List of FG parameters objects*/
	
	Coordinates<double> m_Coord; /*!< Coordinates of study area */
	SpatialMap<double, int> m_Mask; /*!< Map referencing if a point belong (1) or not (0) to the studied area  */
	vector<unsigned> m_MaskCells; /*!< List of the cells belonging to the studied area */
	SpatialStack<double, double> m_SeedMapIn; /*!< Map of seeds produced at the end of succession step that will be dispersed	*/
	SpatialStack<double, double> m_SeedMapOut; /*!< Map of dispersed seeds == succession seed rain*/
	SpatialStack<double, double> m_EnvSuitMap; /*!< Stack of FG environmental suitability maps */
	SpatialStack<double, double> m_EnvSuitRefMap; /*!< Environmental suitability reference maps for current year */
	
	SpatialStack<double, int> m_HabMap; /*!< Stack of habitats mask */
	SpatialStack<double, int> m_DistMap; /*!< Stack of disturbances mask */
	SpatialStack<double, int> m_FireMap; /*!< Stack of disturbances mask */
	SpatialStack<double, double> m_ClimDataMap; /*!< Stack of climatic data mask */
	SpatialMap<double, int> m_tslfMap; /*!< Map referencing the Time Since Last Fire (TSLF) in each cell  */
	SpatialMap<double, double> m_MoistMap; /*!< Moisture index mask  */
	SpatialStack<double, unsigned> m_PostDroughtMap; /*!< Stack of maps referencing for each FG if a point will suffer from post drought effects (0 or 1) */
	SpatialStack<double, unsigned> m_CountDroughtMap; /*!< Stack of maps referencing for each FG the number of consecutive years with drought */
	SpatialStack<double, unsigned> m_IsDroughtMap; /*!< Stack of maps referencing for each FG if a point suffered from drought this year (0 or 1) */
	SpatialStack<double, unsigned> m_ApplyCurrDroughtMap; /*!< Stack of maps referencing for each FG if a point will suffer from current drought effects (0 or 1) this year */
	SpatialStack<double, unsigned> m_ApplyPostDroughtMap; /*!< Stack of maps referencing for each FG if a point will suffer from post drought effects (0 or 1) this year */
	SpatialStack<double, double> m_CondInitMap; /*!< Stack of aliens introduction mask */
	
	SpatialMap<double, SuFatePtr> m_SuccModelMap; /*!< Map of succession models ( stored a pointers ) */
	
	Disp m_DispModel; /*!< Seed dispersal model */
	
	vector< vector<int> > m_AbundHabArray; /*!< Total abundance (for all PFG and all strata) for each habitat type and each year simulated */
	vector< vector<double> > m_EvenHabArray; /*!< Evenness (for all PFG and all strata) for each habitat type and each year simulated */
	vector< vector< vector<double> > > m_HabitatBL; /*!< Quantiles for each habitat type for each statistic of interest */
	
	/*-------------------------------------------*/
	/* Serialization function -------------------*/
	/*-------------------------------------------*/
	
	friend class SuFate;
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int /*version*/)
	{
		cout << ">> Saving/Loading GLOBAL PARAMETERS..." << endl;
		ar & m_glob_params;
		cout << ">> Saving/Loading FG PARAMETERS..." << endl;
		ar & m_FGparams;
		cout << ">> Saving/Loading COORDINATES..." << endl;
		ar & m_Coord;
		cout << ">> Saving/Loading MASK..." << endl;
		ar & m_Mask;
		cout << ">> Saving/Loading MASK CELLS..." << endl;
		ar & m_MaskCells;
		cout << ">> Saving/Loading SEED MAP IN..." << endl;
		ar & m_SeedMapIn;
		cout << ">> Saving/Loading SEED MAP OUT..." << endl;
		ar & m_SeedMapOut;
		cout << ">> Saving/Loading HABITAT SUITABILITY MAPS..." << endl;
		ar & m_EnvSuitMap;
		cout << ">> Saving/Loading HABITAT SUITABILITY REFERENCE MAPS..." << endl;
		ar & m_EnvSuitRefMap;
		cout << ">> Saving/Loading HABITAT MAPS..." << endl;
		ar & m_HabMap;
		cout << ">> Saving/Loading DISTURBANCE MAPS..." << endl;
		ar & m_DistMap;
		cout << ">> Saving/Loading FIRE MAPS..." << endl;
		ar & m_FireMap;
		ar & m_ClimDataMap;
		ar & m_tslfMap;
		ar & m_MoistMap;
		cout << ">> Saving/Loading DROUGHT MAPS..." << endl;
		ar & m_PostDroughtMap;
		ar & m_CountDroughtMap;
		ar & m_IsDroughtMap;
		ar & m_ApplyCurrDroughtMap;
		ar & m_ApplyPostDroughtMap;
		cout << ">> Saving/Loading ALIEN MAPS..." << endl;
		ar & m_CondInitMap;
		cout << ">> Saving/Loading SUCCESSION MODEL MAP..." << endl;
		ar & m_SuccModelMap;
		cout << ">> Saving/Loading DISPERSAL MODEL..." << endl;
		ar & m_DispModel;
		cout << ">> Saving/Loading HABITAT STABILITY MAPS..." << endl;
		ar & m_AbundHabArray;
		ar & m_EvenHabArray;
		ar & m_HabitatBL;
	}
	
	public:
	
	/*-------------------------------------------*/
	/* Constructors -----------------------------*/
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Default constructor
	 *
	 *	SimulMap default constructor => All parameters are set to 0, False or None
	 */
	SimulMap();
	
	/*!
	 *	\brief Full constructor
	 *
	 *	SimulMap full constructor
	 *
	 *	\param file_of_params : FOPL class object containing path to text files
	 * containing well-formatted PFG (LIFE_HISTORY, LIGHT, SOIL, DIST, DISP) or
	 * simulation (MASK, SAVE_DIR, GLOBAL_PARAMETERS, ...) related parameters
	 */
	SimulMap(FOPL file_of_params);
	
	/*-------------------------------------------*/
	/* Destructor -------------------------------*/
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Destructor
	 *
	 *	SimulMap destructor
	 */
	~SimulMap();
	
	/*-------------------------------------------*/
	/* Operators --------------------------------*/
	/*-------------------------------------------*/
	
	bool operator==(SimulMap& o) 
	{
		/* check equality between all simple elements */
		bool is_equal = (m_glob_params == o.m_glob_params &&
		m_FGparams == o.m_FGparams &&
		m_Coord == o.m_Coord &&
		m_Mask == o.m_Mask &&
		m_MaskCells == o.m_MaskCells &&
		m_SeedMapIn == o.m_SeedMapIn &&
		m_SeedMapOut == o.m_SeedMapOut &&
		m_EnvSuitMap == o.m_EnvSuitMap &&
		m_EnvSuitRefMap == o.m_EnvSuitRefMap &&
		m_HabMap == o.m_HabMap &&
		m_DistMap == o.m_DistMap &&
		m_FireMap == o.m_FireMap &&
		m_ClimDataMap == o.m_ClimDataMap &&
		m_tslfMap == o.m_tslfMap &&
		m_MoistMap == o.m_MoistMap &&
		m_PostDroughtMap == o.m_PostDroughtMap &&
		m_CountDroughtMap == o.m_CountDroughtMap &&
		m_IsDroughtMap == o.m_IsDroughtMap &&
		m_ApplyCurrDroughtMap == o.m_ApplyCurrDroughtMap &&
		m_ApplyPostDroughtMap == o.m_ApplyPostDroughtMap &&
		m_CondInitMap == o.m_CondInitMap &&
		m_DispModel == o.m_DispModel &&
		m_AbundHabArray == o.m_AbundHabArray &&
		m_EvenHabArray == o.m_EvenHabArray &&
		m_HabitatBL == o.m_HabitatBL);
		
		/* Compare succession models maps */
		if (is_equal)
		{ // don't do it if there is yet some differences
			is_equal = is_equal && ( *(m_SuccModelMap.getCoordinates()) == *( o.m_SuccModelMap.getCoordinates() ) );
			omp_set_num_threads( m_glob_params.getNbCpus() );
			#pragma omp parallel for schedule(dynamic) if(m_glob_params.getNbCpus()>1)
			for (unsigned i=0; i<m_SuccModelMap.getTotncell(); i++)
			{
				is_equal = is_equal && ( *(m_SuccModelMap(i)) == *(o.m_SuccModelMap(i)) );
			}
		}
		return is_equal;
	}
	
	/*-------------------------------------------*/
	/* Getters & Setters ------------------------*/
	/*-------------------------------------------*/
	
	GSP& getGlobalParameters();
	vector<FG>& getFGparams();
	Coordinates<double>& getCoord();
	SpatialMap<double, int>& getMask();
	vector<unsigned>& getMaskCells();
	SpatialStack<double, double>& getSeedMapIn();
	SpatialStack<double, double>& getSeedMapOut();
	SpatialStack<double, double>& getEnvSuitMap();
	SpatialStack<double, double>& getEnvSuitRefMap();
	SpatialStack<double, int>& getHabMap();
	SpatialStack<double, int>& getDistMap();
	SpatialStack<double, int>& getFireMap();
	SpatialStack<double, double>& getClimDataMap();
	SpatialMap<double, int>& getTslfMap();
	SpatialMap<double, double>& getMoistMap();
	SpatialStack<double, unsigned>& getPostDroughtMap();
	SpatialStack<double, unsigned>& getCountDroughtMap();
	SpatialStack<double, unsigned>& getIsDroughtMap();
	SpatialStack<double, unsigned>& getApplyCurrDroughtMap();
	SpatialStack<double, unsigned>& getApplyPostDroughtMap();
	SpatialStack<double, double>& getCondInitMap();
	SpatialMap<double, SuFatePtr>& getSuccModelMap();
	Disp& getDispModel();
	vector< vector<int> >& getAbundHabArray();
	vector< vector<double> >& getEvenHabArray();
	vector< vector< vector<double> > >& getHabitatBL();
	
	void setGlobalParameters(GSP globalParameters);
	void setFGparams(vector<FG> FGparams);
	void setCoord(Coordinates<double> coord);
	void setMask(SpatialMap<double, int> mask);
	void setMaskCells(vector<unsigned> maskCells);
	void setSeedMapIn(SpatialStack<double, double> seedMapIn);
	void setSeedMapOut(SpatialStack<double, double> seedMapOut);
	void setEnvSuitMap(SpatialStack<double, double> envSuitMap);
	void setEnvSuitRefMap(SpatialStack<double, double> envSuitRefMap);
	void setHabMap(SpatialStack<double, int> habMap);
	void setDistMap(SpatialStack<double, int> distMap);
	void setFireMap(SpatialStack<double, int> fireMap);
	void setClimDataMap(SpatialStack<double, double> climDataMap);
	void setTslfMap(SpatialMap<double, int> tslfMap);
	void setMoistMap(SpatialMap<double, double> moistMap);
	void setPostDroughtMap(SpatialStack<double, unsigned> postDroughtMap);
	void setCountDroughtMap(SpatialStack<double, unsigned> countDroughtMap);
	void setIsDroughtMap(SpatialStack<double, unsigned> isDroughtMap);
	void setApplyCurrDroughtMap(SpatialStack<double, unsigned> applyCurrDroughtMap);
	void setApplyPostDroughtMap(SpatialStack<double, unsigned> applyPostDroughtMap);
	void setCondInitMap(SpatialStack<double, double> condInitMap);
	void setSuccModelMap(SpatialMap<double, SuFatePtr> succModelMap);
	void setDispModel(Disp dispModel);
	void setAbundHabArray(vector< vector<int> > abundHabArray);
	void setEvenHabArray(vector< vector<double> > evenHabArray);
	void setHabitatBL(vector< vector< vector<double> > > habitatBL);
	
	/*-------------------------------------------*/
	/* Other functions --------------------------*/
	/*-------------------------------------------*/
		
	/*!
	 *	\brief Start seeding process
	 *
	 *	This function activates the production of maximal quantity of seeds
	 * (defined by SEEDING_INPUT in GSP) in all pixels for each PFG.
	 */
	void StartSeeding();
	
	/*!
	 *	\brief Start seeding process
	 *
	 *	This function desactivates the production of maximal quantity of seeds
	 * (defined by SEEDING_INPUT in GSP) in all pixels for each PFG.
	 * Seeds will only be produced by currently living and mature individuals.
	 */
	void StopSeeding();
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Change reference maps
	 *
	 *	This function will update all mask maps according to paths to raster
	 * files. Be sure that the order of maps is coherent with the original order
	 * of disturbances or PFG.
	 * 
	 * \param newChangeFile : path to a text file containing a list of path to
	 * raster file(s) (.img or .tif), one line for each new file. Be careful to
	 * the order of those files.
	 * \param typeFile : string indicating the type of files to be changed, and
	 * hence the number of expected file(s) and the type of expected values
	 *   "mask" for simulation map
	 *   "habSuit" for habitat suitability maps
	 *   "dist" for disturbance maps
	 *   "fire" for fire disturbance maps
	 *   "climData" for climatic data maps used for drought index calculation
	 * (drought, elevation, slope)
	 *   "moist" for moisture index maps used for drought module
	 *   "aliens" for introduction maps used for aliens module
	 */
	void DoFileChange(string newChangeFile, string typeFile);
	
	/*!
	 *	\brief Change reference frequencies
	 *
	 *	This function will update all event frequencies.
	 * Be sure that the new number of frequencies is equal to the number of
	 * disturbances of PFG.
	 * 
	 * \param freqChangeFile : path to a text file containing a vector of new
	 * event frequencies, one line for each new year. Be careful to the order of
	 * those values.
	 * \param typeFile : string indicating the type of files to be changed, and
	 * hence the number of expected file(s) and the type of expected values
	 *   "fire" for fire disturbance
	 *   "aliens" for aliens module
	 */
	void DoFreqChange(string freqChangeFile, string typeFile);
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Do succession (demographic) model within each pixel
	 *
	 *	This function runs DoSuccessionPart1 function of each SuFate or SuFateH
	 * model within each study pixel.
	 */
	void DoSuccession();
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Do dispersal model within each pixel
	 *
	 *	This function runs DoDispersalPacket function of dispersal model.
	 * It takes seeds produced by each FG (SeedMapOut) and disperses them all
	 * over the landscape according to the selected dispersal process. Dispersed
	 * seeds are stored (SeedMapIn) and will constitute the seed rain of the
	 * following year.
	 */
	void DoDispersal();
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Do ignition of fire disturbance model
	 *
	 *	This function finds cells in which a fire disturbance will start,
	 * according to the selected fire model (random distribution, ChaoLi
	 * probability).
	 * 
	 * \param dist : id of considered disturbance
	 * \param availCells : vector of cells that can be impacted by the disturbance
	 */
	vector<unsigned int> DoIgnition(int dist, vector<unsigned int> availCells);
	
	/*!
	 *	\brief Do propagation of fire disturbance model
	 *
	 *	This function finds cells in which a fire disturbance will propagate,
	 * starting to previously identified cells (start) and according to the
	 * selected propagation model (probability dependent, based on neighbouring
	 * cells, max amount of fuel, LandClim, ...).
	 * 
	 * \param dist : id of considered disturbance
	 * \param start : vector of cells where there is an ignition of fire
	 * \param availCells : vector of cells that can be impacted by the disturbance
	 */
	vector<unsigned int> DoPropagation(int dist, vector<unsigned int> start, vector<unsigned int> availCells);
	
	/*!
	 *	\brief Update the TimeSinceLastFire mask
	 *
	 *	This function fills a map counting in each cell since when has a fire not
	 * occurred. Pixels impacted this year (burnt) are set to 0, others are
	 * incremented by one.
	 * 
	 * \param burnt : vector of cells impacted by the disturbance
	 */
	void DoUpdateTSLF(vector<unsigned int> burnt);
	
	/*!
	 *	\brief Apply fire disturbance model
	 *
	 *	This function defines, if a fire disturbance should occur this year,
	 * the ignition cells, does the propagation of fire, and applies the 
	 * DoDisturbance function of each SuFate or SuFateH model within each
	 * impacted study pixel.
	 * 
	 * \param yr : current year of simulation
	 */
	void DoFireDisturbance(int yr);
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Apply drought disturbance model
	 *
	 *	This function updates 5 different maps accounting for the drought
	 * process :
	 *   - IsDroughtMap : is there or not drought this year
	 *   - ApplyPostDroughtMap : do drought effects occur next year
	 *   - ApplyCurrDroughtMap : do drought effects occur this year
	 *   - PostDroughtMap : is there post drought mortality
	 *   - CountDroughtMap : how many cumulated years of drought
	 */
	void DoDroughtDisturbance_part1();
	
	/*!
	 *	\brief Apply drought disturbance model
	 *
	 *	This function defines, if a drought disturbance should occur this year,
	 * the impacted cells and applies the DoDisturbance function of each SuFate
	 * or SuFateH model within each impacted study pixel.
	 * 
	 * \param chrono : string indicating if drought effects that should be
	 * applied are before ("prev") or after ("post") succession
	 */
	void DoDroughtDisturbance_part2(string chrono);
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Apply disturbance model
	 *
	 *	This function defines, if a disturbance should occur this year, the
	 * impacted cells and applies the DoDisturbance function of each SuFate
	 * or SuFateH model within each impacted study pixel.
	 * 
	 * \param yr : current year of simulation
	 */
	void DoDisturbance(int yr);
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Apply aliens introduction model
	 *
	 *	This function activates, if invasive introduction should occur this year,
	 * the production of maximal quantity of seeds (defined by SEEDING_INPUT in
	 * GSP) in pixels defined by alien mask for each alien PFG.
	 * 
	 * \param yr : current year of simulation
	 */
	void DoAliensIntroduction(int yr);
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Save total abundance and evenness per habitat in the current year
	 *
	 *	This function stores total abundance and evenness values for each habitat
	 * type (meaning, for pixels contained in one habitat type, sum the
	 * abundances of all FG in all strata) in a 2D vector.
	 * evenness(hab i) = -sum(proportion PFG j * log(proportion PFG j)) /
	 * log(nb PFG - 1)
	 * It will be used for the stability check.
	 */
	void SaveStatsPerHabitat();
	
	/*!
	 *	\brief Compare current total abundance statistics per habitat to baseline
	 * statistics
	 *
	 *	This function compares current mean, standard deviation and coefficient
	 * of variation to the quantiles of the baseline statistics given in
	 * parameter files.
	 * 
	 * \param stat : vector of the current calculated and considered metric
	 * (mean, sd, cv)
	 * \param no : id of considered 
	 * \param hab : id of considered habitat
	 * \param ind_stat : id of considered metric (mean, sd, cv)
	 * \return : true if there is 3 stable states compare to baselin, false
	 * otherwise
	 */
	bool compareQuantiles(vector<double> stat, unsigned no, unsigned hab, unsigned ind_stat);
	
	/*!
	 *	\brief Do stability check model
	 *
	 *	This function calculates current mean, standard deviation and coefficient
	 * of variation using a moving window. These statistics are compared to the
	 * quantiles of the baseline statistics given in parameter files with the
	 * compareQuantiles function.
	 */
	void StabilityCheck();
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Update the current year environmental reference value
	 *
	 *	This function assigns a number between 0 and 1 to each pixel, and for
	 * each PFG. This number will be the new reference for habitat suitability
	 * of the coming year. It is an index of how the following year will be
	 * stressful or not for each PFG.
	 * Two methods are available to draw these numbers :
	 *   - 1 : for each pixel, a number is drawn from uniform distribution, the
	 *         same for all PFG within this pixel
	 *   - 2 : for each PFG, two values are drawn from uniform distribution to
	 *         represent mean and standard deviation. Then, for each pixel, a
	 *         number is drawn from a normal distribution defined by these two
	 *         values.
	 * 
	 * \param option : 1 (random) or 2 (PFG distribution)
	 */
	void UpdateEnvSuitRefMap(unsigned option);
	
	/*!
	 *	\brief Update simulation parameters when starting from backup
	 *
	 *	This function checks and updates simulation parameters when a simulation
	 * is started from the outputs of a previous run (SAVED_STATE), in case some
	 * parameters have changed (e.g. SIMULATION_DURATION, MASK, ...).
	 * 
	 * \param file_of_params : FOPL class object containing path to text files
	 * containing well-formatted PFG (LIFE_HISTORY, LIGHT, SOIL, DIST, DISP) or
	 * simulation (MASK, SAVE_DIR, GLOBAL_PARAMETERS, ...) related parameters
	 */
	void UpdateSimulationParameters(FOPL file_of_params);
	
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Save simulation outputs into raster files
	 *
	 *	This function saves as raster files the different outputs produced by a
	 * FATE simulation :
	 *   - abundances per PFG and per stratum
	 *   - abundances per PFG for all strata
	 *   - light resources (if light competition activated)
	 *   - soil resources (if soil competition activated) 
	 *
	 * \param saveDir : string with the simulation results folder path
	 * \param year : current year of simulation
	 * \param prevFile : path to mask raster file to initiate saved rasters
	 */
	void SaveRasterAbund(string saveDir, int year, string prevFile);
	
};

BOOST_CLASS_VERSION(SimulMap, 0)
#endif //MAP_H

