/*============================================================================*/
/*                     Global Simulation Parameters Class                     */
/*============================================================================*/

/*!
 * \file GSP.h
 * \brief Global Simulation Parameters Class
 * \author Damien Georges
 * \version 1.0
 */
 
#ifndef GSP_H
#define GSP_H
 
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/version.hpp>
#include "Params.h"
#include "FGUtils.h"

using namespace std;


/*!
 * \class GSP
 * \brief Global Simulation Parameters Class
 *
 * This class contains a set of parameters related to the simulation process.
 * Basic parameters are mandatory : they concern simulation timings (simulation
 * duration, seeding duration and timestep, ...) and PFG demography (number of
 * height strata, maximum abundances, ...).
 * If some modules are activated (dispersal, habitat suitability, light, ...),
 * some specific parameters might also be required.
 */

class GSP
{
	private:
	
	int m_NbCpus; /*!< Number of cpus for parallel computing */
	int m_NbFG; /*!< Number of PFGs studied */
	int m_NbStrata; /*!< Number of height strata */
	int m_SimulDuration; /*!< Simulation duration */
	int m_SeedingDuration; /*!< Seeding duration */
	int m_SeedingTimeStep; /*!< Seeding time step */
	int m_SeedingInput; /*!< Number of seeds introduced during seeding */
	int m_MaxByCohort; /*!< Carrying capacity of cohort */
	int m_MaxAbundLow; /*!< Maximum abundance or space a PFG can occupy : low value */
	int m_MaxAbundMedium; /*!< Maximum abundance or space a PFG can occupy : medium value */
	int m_MaxAbundHigh; /*!< Maximum abundance or space a PFG can occupy : high value */
	
	/* Light competition module */
	bool m_DoLightCompetition; /*!< Unable or not Light competition module */
	int m_LightThreshLow; /*!< Threshold to transform PFG abundances into Low light resources */
	int m_LightThreshMedium; /*!< Threshold to transform PFG abundances into Medium light resources */
	
	/* Habitat suitability module */	
	bool m_DoHabSuitability; /*!< Unable or not habitat suitability module */
	int m_HabSuitOption; /*!< Option to draw the habitat suitability ref */
	
	/* Dispersal module */
	bool m_DoDispersal; /*!< Unable or not dispersal module */
	int m_ModeDispersed; /*!< Option to disperse the seeds */
	
	/* Disturbances module */
	bool m_DoDisturbances; /*!< Unable or not disturbance module */
	int m_NbDisturbances; /*!< Number of disturbances */
	int m_NbDistSub; /*!< Number of disturbances subdivision (nb of way to react to dist) */
	vector<int> m_FreqDisturbances; /*!< Frequency of each disturbance in years */
	
	/* Soil competition module */
	bool m_DoSoilCompetition; /*!< Unable or not Soil competition module */
	//int m_NbSoilCat; /*!< Number of soil resources categories */
	
	/* Fire disturbance module */
	bool m_DoFireDisturbances; /*!< Unable or not fire disturbance module */
	int m_NbFireDisturbances; /*!< Number of fire disturbances */
	int m_NbFireSub; /*!< Number of fire disturbances subdivision (nb of way to react to dist) */
	vector<int> m_FreqFires; /*!< Frequency of each fire disturbance in years */
	int m_InitOption; /*!< ignition option (fire) */
	int m_NeighOption; /*!< neighbour option (fire) */
	int m_PropOption; /*!< propagation option (fire) */
	int m_QuotaOption; /*!< quota option (fire) */
	vector<int> m_NbFires; /*!< Number of starting fires fore each fire intensity */
	vector<int> m_PrevData; /*!< Previous data of number of starting fires */
	vector<int> m_CCextent; /*!< Fire size extent in each direction (north, east, south, west) */
	vector<double> m_FireProb; /*!< Probabilities of propagation, depending on fire intensity */
	int m_FlammMax; /*!< Maximum flammability of PFG */
	vector<double> m_LogisInit; /*!< Logistic parameters for the baseline probability of fire ignition */
	vector<double> m_LogisSpread; /*!< Logistic parameters for the baseline probability of fire spread */
	int m_NbClimVar; /*!< Number of climatic variables used for the drought index calculation */
	
	/* Drought disturbance module */
	bool m_DoDroughtDisturbances; /*!< Unable or not drought disturbance module */
	int m_NbDroughtSub; /*!< Number of drought disturbances subdivision (nb of way to react to dist) */
	string m_ChronoPost; /*!< When are applied post drought effects ("prev" or "post" succession) */
	string m_ChronoCurr; /*!< When are applied current drought effects ("prev" or "post" succession) */
	
	/* Habitat stability check */
	bool m_DoHabStabilityCheck; /*!< Unable or not Habitat stability check module */
	int m_NbHabitats; /*!< Number of habitat types */
	
	/* Aliens introduction module */
	bool m_DoAliensDisturbance; /*!< Unable or not aliens introduction module */
	vector<int> m_FreqAliens; /*!< Frequency of each aliens introduction in years */
	
	/*-------------------------------------------*/
	/* Serialization function -------------------*/
	/*-------------------------------------------*/
	
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int /*version*/)
	{
		//cout << "> Serializing Global Simulation Parameters..." << endl;
		ar & m_NbCpus;
		ar & m_NbFG;
		ar & m_NbStrata;
		ar & m_SimulDuration;
		ar & m_SeedingDuration;
		ar & m_SeedingTimeStep;
		ar & m_SeedingInput;
		ar & m_MaxByCohort;
		ar & m_MaxAbundLow;
		ar & m_MaxAbundMedium;
		ar & m_MaxAbundHigh;
		ar & m_DoLightCompetition;
		ar & m_LightThreshLow;
		ar & m_LightThreshMedium;
		ar & m_DoHabSuitability;
		ar & m_HabSuitOption;
		ar & m_DoDispersal;
		ar & m_ModeDispersed;
		ar & m_DoDisturbances;
		ar & m_NbDisturbances;
		ar & m_NbDistSub;
		ar & m_FreqDisturbances;
		ar & m_DoSoilCompetition;
		ar & m_DoFireDisturbances;
		ar & m_NbFireDisturbances;
		ar & m_NbFireSub;
		ar & m_FreqFires;
		ar & m_InitOption;
		ar & m_NeighOption;
		ar & m_PropOption;
		ar & m_QuotaOption;
		ar & m_NbFires;
		ar & m_PrevData;
		ar & m_CCextent;
		ar & m_FireProb;
		ar & m_FlammMax;
		ar & m_LogisInit;
		ar & m_LogisSpread;
		ar & m_NbClimVar;
		ar & m_DoDroughtDisturbances;
		ar & m_NbDroughtSub;
		ar & m_ChronoPost;
		ar & m_ChronoCurr;
		ar & m_DoHabStabilityCheck;
		ar & m_NbHabitats;
		ar & m_DoAliensDisturbance;
		ar & m_FreqAliens;
	}
	
	public:
	
	/*-------------------------------------------*/
	/* Constructors -----------------------------*/
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Default constructor
	 *
	 *	GSP default constructor => All parameters are set to 0, False or None
	 */
	GSP();
	
	/*!
	 *	\brief Full constructor
	 *
	 *	GSP full constructor
	 *
	 *	\param globalParamsFile : path to text file containing well-formatted
	 * global simulation related parameters
	 */
	GSP(const string globalParamsFile);
	
	/*!
	 *	\brief Full constructor
	 *
	 *	GSP full constructor
	 *
	 *	\param nbCpus : number of CPUs required for simulation
	 *	\param nbFG : number of Functional Groups involved
	 *	\param nbStrata : number of height strata
	 *	\param simulDuration : simulation duration (in years)
	 *	\param seedingDuration : seeding duration (in years)
	 *	\param seedingTimeStep : seeding time step (in years)
	 *	\param seedingInput : number of seeds dispersed each year during seeding
	 *	\param maxByCohort : carrying capacity of a cohort
	 *	\param maxAbundLow : maximum abundance or space a PFG can occupy : low
	 * value
	 *	\param maxAbundMedium : maximum abundance or space a PFG can occupy :
	 * medium value
	 *	\param maxAbundHigh : maximum abundance or space a PFG can occupy :
	 * high value
	 *	\param doLightCompetition : unable or not Light competition module
	 *	\param lightThreshLow : threshold to transform PFG abundances into Low
	 * light resources
	 *	\param lightThreshMedium : threshold to transform PFG abundances into
	 * Medium light resources
	 *	\param doHabSuitability : unable or not Habitat suitability module
	 *	\param habSuitOption : option to draw the habitat suitability ref
	 *	\param doDispersal : unable or not Dispersal module
	 * \param modeDispersed : option to disperse the seeds
	 *	\param doDisturbances : unable or not Disturbances module
	 *	\param nbDisturbances : number of disturbances involved
	 *	\param nbDistSub : number of way a FG can react to a disturbance
	 * \param freqDisturbances : the frequency of each disturbance
	 *	\param doSoilCompetition : unable or not Soil competition module
	 *	\param doFireDisturbances : unable or not Fire disturbances module
	 *	\param nbFireDisturbances : number of fire disturbances involved
	 *	\param nbFireSub : number of way a FG can react to a fire disturbance
	 *	\param freqFires : the frequency of each fire disturbance
	 * \param initOption : ignition option (fire)
	 * \param neighOption : neighbour option (fire)
	 * \param propOption : propagation option (fire)
	 * \param quotaOption : quota option (fire)
	 *	\param nbFires : number of starting fires fore each fire intensity
	 *	\param prevData : previous data of number of starting fires
	 *	\param ccExtent : fire size extent in each direction (north, east, south,
	 * west)
	 *	\param fireProb : probabilities of propagation, depending on fire
	 * intensity
	 *	\param flammMax : maximum flammability of PFG
	 *	\param logisInit : logistic parameters for the baseline probability of
	 * fire ignition
	 *	\param logisSpread : logistic parameters for the baseline probability of
	 * fire spread
	 *	\param nbClimVar : number of climatic variables used for the drought
	 * index calculation
	 *	\param doDroughtDisturbances : unable or not Drought disturbances module
	 *	\param nbDroughtSub : number of way a FG can react to a drought
	 * disturbance
	 *	\param chronoPost : when are applied post drought effects ("prev" or
	 * "post" succession)
	 *	\param chronoCurr : when are applied current drought effects ("prev" or
	 * "post" succession)
	 *	\param doHabStabilityCheck : unable or not Habitat stability check module
	 *	\param nbHabitats : number of habitat types involved
	 *	\param doAliensDisturbance : unable or not Aliens introduction module
	 *	\param freqAliens : the frequency of each aliens introduction
	 */
	GSP(const int& nbCpus,
	const int& nbFG,
	const int& nbStrata,
	const int& simulDuration,
	const int& seedingDuration,
	const int& seedingTimeStep,
	const int& seedingInput,
	const int& maxByCohort,
	const int& maxAbundLow,
	const int& maxAbundMedium,
	const int& maxAbundHigh,
	const bool& doLightCompetition,
	const int& lightThreshLow,
	const int& lightThreshMedium,
	const bool& doHabSuitability,
	const int& habSuitOption,
	const bool& doDispersal,
	const int& modeDispersed,
	const bool& doDisturbances,
	const int& nbDisturbances,
	const int& nbDistSub,
	const vector<int>& freqDisturbances,
	const bool& doSoilCompetition,
	const bool& doFireDisturbances,
	const int& nbFireDisturbances,
	const int& nbFireSub,
	const vector<int>& freqFires,
	const int& initOption,
	const int& neighOption,
	const int& propOption,
	const int& quotaOption,
	const vector<int>& nbFires,
	const vector<int>& prevData,
	const vector<int>& ccExtent,
	const vector<double>& fireProb,
	const int& flammMax,
	const vector<double>& logisInit,
	const vector<double>& logisSpread,
	const int& nbClimVar,
	const bool& doDroughtDisturbances,
	const int& nbDroughtSub,
	const string& chronoPost,
	const string& chronoCurr,
	const bool& doHabStabilityCheck,
	const int& nbHabitats,
	const bool& doAliensDisturbance,
	const vector<int>& freqAliens);
	
	/*-------------------------------------------*/
	/* Destructor -------------------------------*/
	/*-------------------------------------------*/
	
	/*!
	 *	\brief Destructor
	 *
	 *	GSP destructor
	 */
	~GSP();
	
	/*-------------------------------------------*/
	/* Operators --------------------------------*/
	/*-------------------------------------------*/
	
	bool operator==(const GSP& o) const
	{
		return ( m_NbCpus == o.m_NbCpus &&
		m_NbFG == o.m_NbFG &&
		m_NbStrata == o.m_NbStrata &&
		m_SimulDuration == o.m_SimulDuration &&
		m_SeedingDuration == o.m_SeedingDuration &&
		m_SeedingTimeStep == o.m_SeedingTimeStep &&
		m_SeedingInput == o.m_SeedingInput &&
		m_MaxByCohort == o.m_MaxByCohort &&
		m_MaxAbundLow == o.m_MaxAbundLow &&
		m_MaxAbundMedium == o.m_MaxAbundMedium &&
		m_MaxAbundHigh == o.m_MaxAbundHigh &&
		m_DoLightCompetition == o.m_DoLightCompetition &&
		m_LightThreshLow == o.m_LightThreshLow &&
		m_LightThreshMedium == o.m_LightThreshMedium &&
		m_DoHabSuitability == o.m_DoHabSuitability &&
		m_HabSuitOption == o.m_HabSuitOption &&
		m_DoDispersal == o.m_DoDispersal &&
		m_ModeDispersed == o.m_ModeDispersed &&
		m_DoDisturbances == o.m_DoDisturbances &&
		m_NbDisturbances == o.m_NbDisturbances &&
		m_NbDistSub == o.m_NbDistSub &&
		m_FreqDisturbances == o.m_FreqDisturbances &&
		m_DoSoilCompetition == o.m_DoSoilCompetition &&
		m_DoFireDisturbances == o.m_DoFireDisturbances &&
		m_NbFireDisturbances == o.m_NbFireDisturbances &&
		m_NbFireSub == o.m_NbFireSub &&
		m_FreqFires == o.m_FreqFires &&
		m_InitOption == o.m_InitOption &&
		m_NeighOption == o.m_NeighOption &&
		m_PropOption == o.m_PropOption &&
		m_QuotaOption == o.m_QuotaOption &&
		m_NbFires == o.m_NbFires &&
		m_PrevData == o.m_PrevData &&
		m_CCextent == o.m_CCextent &&
		m_FireProb == o.m_FireProb &&
		m_FlammMax == o.m_FlammMax &&
		m_LogisInit == o.m_LogisInit &&
		m_LogisSpread == o.m_LogisSpread &&
		m_NbClimVar == o.m_NbClimVar &&
		m_DoDroughtDisturbances == o.m_DoDroughtDisturbances &&
		m_NbDroughtSub == o.m_NbDroughtSub &&
		m_ChronoPost == o.m_ChronoPost &&
		m_ChronoCurr == o.m_ChronoCurr &&
		m_DoHabStabilityCheck == o.m_DoHabStabilityCheck &&
		m_NbHabitats == o.m_NbHabitats &&
		m_DoAliensDisturbance == o.m_DoAliensDisturbance &&
		m_FreqAliens == o.m_FreqAliens);
	}
	
	/*-------------------------------------------*/
	/* Getters & Setters ------------------------*/
	/*-------------------------------------------*/
	
	const int& getNbCpus() const;
	const int& getNbFG() const;
	const int& getNbStrata() const;
	const int& getSimulDuration() const;
	const int& getSeedingDuration() const;
	const int& getSeedingTimeStep() const;
	const int& getSeedingInput() const;
	const int& getMaxByCohort() const;
	const int& getMaxAbundLow() const;
	const int& getMaxAbundMedium() const;
	const int& getMaxAbundHigh() const;
	const bool& getDoLightCompetition() const;
	const int& getLightThreshLow() const;
	const int& getLightThreshMedium() const;
	const bool& getDoHabSuitability() const;
	const int& getHabSuitOption() const;
	const bool& getDoDispersal() const;
	const int& getModeDispersed() const;
	const bool& getDoDisturbances() const;
	const int& getNbDisturbances() const;
	const int& getNbDistSub() const;
	const vector<int>& getFreqDisturbances() const;
	const bool& getDoSoilCompetition() const;
	const bool& getDoFireDisturbances() const;
	const int& getNbFireDisturbances() const;
	const int& getNbFireSub() const;
	const vector<int>& getFreqFires() const;
	const int& getInitOption() const;
	const int& getNeighOption() const;
	const int& getPropOption() const;
	const int& getQuotaOption() const;
	const vector<int>& getNbFires() const;
	const vector<int>& getPrevData() const;
	const vector<int>& getCCextent() const;
	const vector<double>& getFireProb() const;
	const int& getFlammMax() const;
	const vector<double>& getLogisInit() const;
	const vector<double>& getLogisSpread() const;
	const int& getNbClimVar() const;
	const bool& getDoDroughtDisturbances() const;
	const int& getNbDroughtSub() const;
	const string& getChronoPost() const;
	const string& getChronoCurr() const;
	const bool& getDoHabStabilityCheck() const;
	const int& getNbHabitats() const;
	const bool& getDoAliensDisturbance() const;
	const vector<int>& getFreqAliens() const;
	
	void setNbCpus(const int& nbCpus);
	void setNbFG(const int& nbFG);
	void setNbStrata(const int& nbStrata);
	void setSimulDuration(const int& simulDuration);
	void setSeedingDuration(const int& seedingDuration);
	void setSeedingTimeStep(const int& seedingTimeStep);
	void setSeedingInput(const int& seedingInput);
	void setMaxByCohort(const int& maxByCohort);
	void setMaxAbundLow(const int& maxAbundLow);
	void setMaxAbundMedium(const int& maxAbundMedium);
	void setMaxAbundHigh(const int& maxAbundHigh);
	void setDoLightCompetition(const bool& doLightCompetition);
	void setLightThreshLow(const int& lightThreshLow);
	void setLightThreshMedium(const int& lightThreshMedium);
	void setDoHabSuitability(const bool& doHabSuitability);
	void setHabSuitOption(const int& habSuitOption);
	void setDoDispersal(const bool& doDispersal);
	void setModeDispersed(const int& modeDispersed);
	void setDoDisturbances(const bool& doDisturbances);
	void setNbDisturbances(const int& nbDisturbances);
	void setNbDistSub(const int& nbDistSub);
	void setFreqDisturbances(const vector<int>& freqDisturbances);
	void setDoSoilCompetition(const bool& doSoilCompetition);
	void setDoFireDisturbances(const bool& doFireDisturbances);
	void setNbFireDisturbances(const int& nbFireDisturbances);
	void setNbFireSub(const int& nbFireSub);
	void setFreqFires(const vector<int>& freqFires);
	void setInitOption(const int& initOption);
	void setNeighOption(const int& neighOption);
	void setPropOption(const int& propOption);
	void setQuotaOption(const int& quotaOption);
	void setNbFires(const vector<int>& nbFires);
	void setPrevData(const vector<int>& prevData);
	void setCCextent(const vector<int>& ccExtent);
	void setFireProb(const vector<double>& fireProb);
	void setFlammMax(const int& flammMax);
	void setLogisInit(const vector<double>& logisInit);
	void setLogisSpread(const vector<double>& logisSpread);
	void setNbClimVar(const int& nbClimVar);
	void setDoDroughtDisturbances(const bool& doDroughtDisturbances);
	void setNbDroughtSub(const int& nbDroughtSub);
	void setChronoPost(const string& chronoPost);
	void setChronoCurr(const string& chronoCurr);
	void setDoHabStabilityCheck(const bool& doHabStabilityCheck);
	void setNbHabitats(const int& nbHabitats);
	void setDoAliensDisturbance(const bool& doAliensDisturbance);
	void setFreqAliens(const vector<int>& freqAliens);
	
	/*-------------------------------------------*/
	/* Other functions --------------------------*/
	/*-------------------------------------------*/
	
	void show();
	
	/*!
	 *	\brief Convert Abundance classes to integer
	 *
	 *	This functions converts a value from enum Abund to an integer :
	 *   - ANone -> 0
	 *   - ALow -> m_MaxAbundLow
	 *   - AMedium -> m_MaxAbundMedium
	 *   - AHigh -> m_MaxAbundHigh
	 * 
	 * \param abund : a value from enum Abund (ANone, ALow, AMedium, AHigh)
	 *	\return : an integer corresponding to a value from enum Abund
	 */
	int AbundToInt(Abund abund);

};

BOOST_CLASS_VERSION(GSP, 0)
#endif //GSP_H

