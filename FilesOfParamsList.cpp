#include "FilesOfParamsList.h"

using namespace std;


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Constructors                                                                                    */
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

FOPL::FOPL() : 
m_GlobSimulParams(""), /* Global simulation parameters */
m_SavedState(""), m_SavingDir(""), m_SummArrSavingTimes(""), m_ObjectsSavingTimes(""), /* Saving parameters */
m_Mask(""),m_HabitatMask(1,""), m_DisturbMask(1,""), m_FireMask(1,""), m_ClimDataMask(1,""), m_MoistIndexMask(""), m_SlopeMask(""), /* Spatial parameters */
m_MaskChangeMasks(1,""), m_MaskChangeTimes(""), m_HabChangeMasks(1,""), m_HabChangeTimes(""), m_DistChangeMasks(1,""), m_DistChangeTimes(""), /* Simulation Changes parameters */
m_FireChangeMasks(1,""), m_FireChangeTimes(""), m_FireChangeFrequencies(1,""), m_FireFreqChangeTimes(""), m_ClimDataChangeMasks(1,""), 
m_ClimDataChangeTimes(""), m_MoistIndexChangeMasks(1,""), m_MoistIndexChangeTimes(""),
m_AliensChangeMasks(1,""), m_AliensChangeTimes(""), m_AliensChangeFrequencies(1,""), m_AliensFreqChangeTimes(""), /* Aliens introduction parameters */
m_FGLifeHistory(1,""), m_FGLight(1,""), m_FGHabSuitMaps(1,""), m_FGDispersal(1,""), /* FG specific parameters */
m_FGDisturbance(1,""), m_FGSoil(1,""), m_FGFire(1,""), m_FGDrought(1,""), m_FGCondInitMaps(1,""), /* FG specific parameters */
m_HabitatBLstats(1,"") /* Habitat baseline statistics */
{
	/* Nothing to do */
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

FOPL::FOPL(string paramSimulFile)
{
	testFileExist("paramSimulFile", paramSimulFile, false);
	
	/* Global simulation parameters */
	m_GlobSimulParams = ReadParamsWithinFile(paramSimulFile, "GLOBAL_PARAMS")[0];
	
	/* Saving parameters */
	m_SavedState = ReadParamsWithinFile(paramSimulFile, "SAVED_STATE")[0];
	m_SavingDir = ReadParamsWithinFile(paramSimulFile, "SAVE_DIR")[0];
	m_SummArrSavingTimes = ReadParamsWithinFile(paramSimulFile, "ARRAYS_SAVING_YEARS")[0];
	m_ObjectsSavingTimes = ReadParamsWithinFile(paramSimulFile, "OBJECTS_SAVING_YEARS")[0];
	
	/* Spatial parameters */
	m_Mask = ReadParamsWithinFile(paramSimulFile, "MASK")[0];
	m_HabitatMask = ReadParamsWithinFile(paramSimulFile, "HAB_MASK");
	m_DisturbMask = ReadParamsWithinFile(paramSimulFile, "DIST_MASK");
	m_FireMask = ReadParamsWithinFile(paramSimulFile, "FIRE_MASK");
	m_ClimDataMask = ReadParamsWithinFile(paramSimulFile, "CLIM_DATA_MASK");
	m_MoistIndexMask = ReadParamsWithinFile(paramSimulFile, "MOIST_MASK")[0];
	m_SlopeMask = ReadParamsWithinFile(paramSimulFile, "SLOPE_MASK")[0];
	
	/* Simulation Timing parameters */
	m_MaskChangeMasks = ReadParamsWithinFile(paramSimulFile, "MASK_CHANGE_MASK");
	m_MaskChangeTimes = ReadParamsWithinFile(paramSimulFile, "MASK_CHANGE_TIME")[0];
	m_HabChangeMasks = ReadParamsWithinFile(paramSimulFile, "HAB_CHANGE_MASK");
	m_HabChangeTimes = ReadParamsWithinFile(paramSimulFile, "HAB_CHANGE_TIME")[0];
	m_DistChangeMasks = ReadParamsWithinFile(paramSimulFile, "DIST_CHANGE_MASK");
	m_DistChangeTimes = ReadParamsWithinFile(paramSimulFile, "DIST_CHANGE_TIME")[0];
	m_FireChangeMasks = ReadParamsWithinFile(paramSimulFile, "FIRE_CHANGE_MASK");
	m_FireChangeTimes = ReadParamsWithinFile(paramSimulFile, "FIRE_CHANGE_TIME")[0];
	m_FireChangeFrequencies = ReadParamsWithinFile(paramSimulFile, "FIRE_CHANGE_FREQUENCIES");
	m_FireFreqChangeTimes = ReadParamsWithinFile(paramSimulFile, "FIRE_FREQ_CHANGE_TIME")[0];
	m_ClimDataChangeMasks = ReadParamsWithinFile(paramSimulFile, "CLIM_DATA_CHANGE_MASK");
	m_ClimDataChangeTimes = ReadParamsWithinFile(paramSimulFile, "CLIM_DATA_CHANGE_TIME")[0];
	m_MoistIndexChangeMasks = ReadParamsWithinFile(paramSimulFile, "MOIST_CHANGE_MASK");
	m_MoistIndexChangeTimes = ReadParamsWithinFile(paramSimulFile, "MOIST_CHANGE_TIME")[0];
	
	/* FG specific parameters */
	m_FGLifeHistory = ReadParamsWithinFile(paramSimulFile, "PFG_LIFE_HISTORY_PARAMS");
	m_FGLight = ReadParamsWithinFile(paramSimulFile, "PFG_LIGHT_PARAMS");
	m_FGHabSuitMaps = ReadParamsWithinFile(paramSimulFile, "PFG_HAB_MASK");
	m_FGDispersal = ReadParamsWithinFile(paramSimulFile, "PFG_DISPERSAL_PARAMS");
	m_FGDisturbance = ReadParamsWithinFile(paramSimulFile, "PFG_DISTURBANCES_PARAMS");
	m_FGSoil = ReadParamsWithinFile(paramSimulFile, "PFG_SOIL_PARAMS");
	m_FGFire = ReadParamsWithinFile(paramSimulFile, "PFG_FIRES_PARAMS");
	m_FGDrought = ReadParamsWithinFile(paramSimulFile, "PFG_DROUGHT_PARAMS");
	m_FGCondInitMaps = ReadParamsWithinFile(paramSimulFile, "ALIENS_MASK");

	/* Aliens introduction parameters */
	m_AliensChangeMasks = ReadParamsWithinFile(paramSimulFile, "ALIENS_CHANGE_MASK");
	m_AliensChangeTimes = ReadParamsWithinFile(paramSimulFile, "ALIENS_CHANGE_TIME")[0];
	m_AliensChangeFrequencies = ReadParamsWithinFile(paramSimulFile, "ALIENS_CHANGE_FREQUENCIES");
	m_AliensFreqChangeTimes = ReadParamsWithinFile(paramSimulFile, "ALIENS_FREQ_CHANGE_TIME")[0];

	/* Habitat baseline statistics */
	m_HabitatBLstats = ReadParamsWithinFile(paramSimulFile, "HAB_BASELINE_STATS");
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Destructor                                                                                      */
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

FOPL::~FOPL()
{
	/* Nothing to do */
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Getters & Setters                                                                               */
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

const string& FOPL::getGlobSimulParams() const{ return  m_GlobSimulParams; }
const string& FOPL::getSavedState() const{ return  m_SavedState; }
const string& FOPL::getSavingDir() const{ return  m_SavingDir; }
const string& FOPL::getSummArrSavingTimes() const{ return  m_SummArrSavingTimes; }
const string& FOPL::getObjectsSavingTimes() const{ return  m_ObjectsSavingTimes; }
const string& FOPL::getMask() const{ return  m_Mask; }
const vector<string>& FOPL::getHabitatMask() const{ return  m_HabitatMask; }
const vector<string>& FOPL::getDisturbMask() const{ return  m_DisturbMask; }
const vector<string>& FOPL::getFireMask() const{ return  m_FireMask; }
const vector<string>& FOPL::getClimDataMask() const{ return  m_ClimDataMask; }
const string& FOPL::getMoistIndexMask() const{ return  m_MoistIndexMask; }
const string& FOPL::getSlopeMask() const{ return  m_SlopeMask; }
const vector<string>& FOPL::getMaskChangeMasks() const{ return  m_MaskChangeMasks; }
const string& FOPL::getMaskChangeTimes() const{ return  m_MaskChangeTimes; }
const vector<string>& FOPL::getHabChangeMasks() const{ return  m_HabChangeMasks; }
const string& FOPL::getHabChangeTimes() const{ return  m_HabChangeTimes; }
const vector<string>& FOPL::getDistChangeMasks() const{ return  m_DistChangeMasks; }
const string& FOPL::getDistChangeTimes() const{ return  m_DistChangeTimes; }
const vector<string>& FOPL::getFireChangeMasks() const{ return  m_FireChangeMasks; }
const string& FOPL::getFireChangeTimes() const{ return  m_FireChangeTimes; }
const vector<string>& FOPL::getFireChangeFrequencies() const{ return  m_FireChangeFrequencies; }
const string& FOPL::getFireFreqChangeTimes() const{ return  m_FireFreqChangeTimes; }
const vector<string>& FOPL::getClimDataChangeMasks() const{ return  m_ClimDataChangeMasks; }
const string& FOPL::getClimDataChangeTimes() const{ return  m_ClimDataChangeTimes; }
const vector<string>& FOPL::getMoistIndexChangeMasks() const{ return  m_MoistIndexChangeMasks; }
const string& FOPL::getMoistIndexChangeTimes() const{ return  m_MoistIndexChangeTimes; }
const vector<string>& FOPL::getFGLifeHistory() const{ return  m_FGLifeHistory; }
const vector<string>& FOPL::getFGLight() const{ return  m_FGLight; }
const vector<string>& FOPL::getFGHabSuitMaps() const{ return  m_FGHabSuitMaps; }
const vector<string>& FOPL::getFGDispersal() const{ return  m_FGDispersal; }
const vector<string>& FOPL::getFGDisturbance() const{ return  m_FGDisturbance; }
const vector<string>& FOPL::getFGSoil() const{ return  m_FGSoil; }
const vector<string>& FOPL::getFGFire() const{ return  m_FGFire; }
const vector<string>& FOPL::getFGDrought() const{ return  m_FGDrought; }
const vector<string>& FOPL::getFGCondInitMaps() const{ return  m_FGCondInitMaps; }
const vector<string>& FOPL::getHabitatBLstats() const{ return  m_HabitatBLstats; }
const vector<string>& FOPL::getAliensChangeMasks() const{ return  m_AliensChangeMasks; }
const string& FOPL::getAliensChangeTimes() const{ return  m_AliensChangeTimes; }
const vector<string>& FOPL::getAliensChangeFrequencies() const{ return  m_AliensChangeFrequencies; }
const string& FOPL::getAliensFreqChangeTimes() const{ return  m_AliensFreqChangeTimes; }

void FOPL::setGlobSimulParams(const string& globSimulParams){ m_GlobSimulParams = globSimulParams; }
void FOPL::setSavedState(const string& savedState){ m_SavedState = savedState; }
void FOPL::setSavingDir(const string& savingDir){ m_SavingDir = savingDir; }
void FOPL::setSummArrSavingTimes(const string& summArrSavingTimes){ m_SummArrSavingTimes = summArrSavingTimes; }
void FOPL::setObjectsSavingTimes(const string& objectsSavingTimes){ m_ObjectsSavingTimes = objectsSavingTimes; }
void FOPL::setMask(const string& mask){ m_Mask = mask; }
void FOPL::setHabitatMask(const vector<string>& habitatMask){ m_HabitatMask = habitatMask; }
void FOPL::setDisturbMask(const vector<string>& disturbMask){ m_DisturbMask = disturbMask; }
void FOPL::setFireMask(const vector<string>& fireMask){ m_FireMask = fireMask; }
void FOPL::setClimDataMask(const vector<string>& climDataMask){ m_ClimDataMask = climDataMask; }
void FOPL::setMoistIndexMask(const string& moistIndexMask){ m_MoistIndexMask = moistIndexMask; }
void FOPL::setSlopeMask(const string& slopeMask){ m_SlopeMask = slopeMask; }
void FOPL::setMaskChangeMasks(const vector<string>& maskChangeMasks){ m_MaskChangeMasks = maskChangeMasks; }
void FOPL::setMaskChangeTimes(const string& maskChangeTimes){ m_MaskChangeTimes = maskChangeTimes; }
void FOPL::setHabChangeMasks(const vector<string>& habChangeMasks){ m_HabChangeMasks = habChangeMasks; }
void FOPL::setHabChangeTimes(const string& habChangeTimes){ m_HabChangeTimes = habChangeTimes; }
void FOPL::setDistChangeMasks(const vector<string>& distChangeMasks){ m_DistChangeMasks = distChangeMasks; }
void FOPL::setDistChangeTimes(const string& distChangeTimes){ m_DistChangeTimes = distChangeTimes; }
void FOPL::setFireChangeMasks(const vector<string>& fireChangeMasks){ m_FireChangeMasks = fireChangeMasks; }
void FOPL::setFireChangeTimes(const string& fireChangeTimes){ m_FireChangeTimes =fireChangeTimes; }
void FOPL::setFireChangeFrequencies(const vector<string>& fireChangeFrequencies){ m_FireChangeFrequencies = fireChangeFrequencies; }
void FOPL::setFireFreqChangeTimes(const string& fireFreqChangeTimes){ m_FireFreqChangeTimes = fireFreqChangeTimes; }
void FOPL::setClimDataChangeMasks(const vector<string>& climDataChangeMasks){ m_ClimDataChangeMasks = climDataChangeMasks; }
void FOPL::setClimDataChangeTimes(const string& climDataChangeTimes){ m_ClimDataChangeTimes = climDataChangeTimes; }
void FOPL::setMoistIndexChangeMasks(const vector<string>& moistIndexChangeMasks){ m_MoistIndexChangeMasks = moistIndexChangeMasks; }
void FOPL::setMoistIndexChangeTimes(const string& moistIndexChangeTimes){ m_MoistIndexChangeTimes = moistIndexChangeTimes; }
void FOPL::setFGLifeHistory(const vector<string>& fgLifeHistory){ m_FGLifeHistory = fgLifeHistory; }
void FOPL::setFGLight(const vector<string>& fgLight){ m_FGLight = fgLight; }
void FOPL::setFGHabSuitMaps(const vector<string>& fgHabSuitMaps){ m_FGHabSuitMaps = fgHabSuitMaps; }
void FOPL::setFGDispersal(const vector<string>& fgDispersal){ m_FGDispersal = fgDispersal; }
void FOPL::setFGDisturbance(const vector<string>& fgDisturbance){ m_FGDisturbance = fgDisturbance; }
void FOPL::setFGSoil(const vector<string>& fgSoil){ m_FGSoil = fgSoil; }
void FOPL::setFGFire(const vector<string>& fgFire){ m_FGFire = fgFire; }
void FOPL::setFGDrought(const vector<string>& fgDrought){ m_FGDrought = fgDrought; }
void FOPL::setFGCondInitMaps(const vector<string>& fgCondInitMaps){ m_FGCondInitMaps = fgCondInitMaps; }
void FOPL::setHabitatBLstats(const vector<string>& habitatBLstats){ m_HabitatBLstats = habitatBLstats; }
void FOPL::setAliensChangeMasks(const vector<string>& aliensChangeMasks){ m_AliensChangeMasks = aliensChangeMasks; }
void FOPL::setAliensChangeTimes(const string& aliensChangeTimes){ m_AliensChangeTimes = aliensChangeTimes; }
void FOPL::setAliensChangeFrequencies(const vector<string>& aliensChangeFrequencies){ m_AliensChangeFrequencies = aliensChangeFrequencies; }
void FOPL::setAliensFreqChangeTimes(const string& aliensFreqChangeTimes){ m_AliensFreqChangeTimes = aliensFreqChangeTimes; }

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Other functions		                                                                           */
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void FOPL::show()
{
	cout << endl;
	cout << "List of parameters files paths:" << endl;
	cout << endl;
	
	/* Global simulation parameters */
	cout << "m_GlobSimulParams = " << m_GlobSimulParams << endl;
	
	/* Saving parameters */
	cout << "m_SavingDir = " << m_SavingDir << endl;
	cout << "m_SummArrSavingTimes = " << m_SummArrSavingTimes << endl;
	cout << "m_ObjectsSavingTimes = " << m_ObjectsSavingTimes << endl;
	
	/* Spatial parameters */
	cout << "m_Mask = " << m_Mask << endl;
	cout << "m_HabitatMask = ";
	copy(m_HabitatMask.begin(), m_HabitatMask.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_DisturbMask = ";
	copy(m_DisturbMask.begin(), m_DisturbMask.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FireMask = ";
	copy(m_FireMask.begin(), m_FireMask.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_ClimDataMask = ";
	copy(m_ClimDataMask.begin(), m_ClimDataMask.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_MoistIndexMask = " << m_MoistIndexMask << endl;
	cout << "m_SlopeMask = " << m_SlopeMask << endl;
	
	/* Simulation Timing parameters */
	cout << "m_MaskChangeMasks = ";
	copy(m_MaskChangeMasks.begin(), m_MaskChangeMasks.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_MaskChangeTimes = " << m_MaskChangeTimes << endl;
	cout << "m_HabChangeMasks = ";
	copy(m_HabChangeMasks.begin(), m_HabChangeMasks.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_HabChangeTimes = " << m_HabChangeTimes << endl;
	cout << "m_DistChangeMasks = ";
	copy(m_DistChangeMasks.begin(), m_DistChangeMasks.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_DistChangeTimes = " << m_DistChangeTimes << endl;
	cout << "m_FireChangeMasks = ";
	copy(m_FireChangeMasks.begin(), m_FireChangeMasks.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FireChangeTimes = " << m_FireChangeTimes << endl;
	cout << "m_FireChangeFrequencies = ";
	copy(m_FireChangeFrequencies.begin(), m_FireChangeFrequencies.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FireFreqChangeTimes = " << m_FireFreqChangeTimes << endl;
	cout << "m_ClimDataChangeMasks = ";
	copy(m_ClimDataChangeMasks.begin(), m_ClimDataChangeMasks.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_ClimDataChangeTimes = " << m_ClimDataChangeTimes << endl;
	cout << "m_MoistIndexChangeMasks = ";
	copy(m_MoistIndexChangeMasks.begin(), m_MoistIndexChangeMasks.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_MoistIndexChangeTimes = " << m_MoistIndexChangeTimes << endl;
	
	/* FG specific parameters */
	cout << "m_FGLifeHistory = ";
	copy(m_FGLifeHistory.begin(), m_FGLifeHistory.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FGLight = ";
	copy(m_FGLight.begin(), m_FGLight.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FGHabSuitMaps = ";
	copy(m_FGHabSuitMaps.begin(), m_FGHabSuitMaps.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FGDispersal = ";
	copy(m_FGDispersal.begin(), m_FGDispersal.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FGDisturbance = ";
	copy(m_FGDisturbance.begin(), m_FGDisturbance.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FGSoil = ";
	copy(m_FGSoil.begin(), m_FGSoil.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FGFire = ";
	copy(m_FGFire.begin(), m_FGFire.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FGDrought = ";
	copy(m_FGDrought.begin(), m_FGDrought.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_FGCondInitMaps = ";
	copy(m_FGCondInitMaps.begin(), m_FGCondInitMaps.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	
	/* Habitat baseline statistics */
	cout << "m_HabitatBLstats = ";
	copy(m_HabitatBLstats.begin(), m_HabitatBLstats.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	
	/* Aliens introduction parameters */
	cout << "m_AliensChangeMasks = ";
	copy(m_AliensChangeMasks.begin(), m_AliensChangeMasks.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_AliensChangeTimes = " << m_AliensChangeTimes << endl;
	cout << "m_AliensChangeFrequencies = ";
	copy(m_AliensChangeFrequencies.begin(), m_AliensChangeFrequencies.end(), ostream_iterator<string>(cout, " "));
	cout << endl;
	cout << "m_AliensFreqChangeTimes = " << m_AliensFreqChangeTimes << endl;
	cout << endl;
} // end of show()


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void FOPL::checkCorrectParams_light()
{
	testDirExist("--SAVE_DIR--",m_SavingDir+"/LIGHT/");
	testFileExist("--PFG_LIGHT_PARAMS--",m_FGLight, false);
}

void FOPL::checkCorrectParams_habSuit()
{
	testFileExist("--PFG_HAB_MASK--",m_FGHabSuitMaps, false);
	testFileExist("--HAB_CHANGE_TIME--",m_HabChangeTimes, (strcmp(m_HabChangeTimes.c_str(),"0")==0));
	testFileExist("--HAB_CHANGE_MASK--",m_HabChangeMasks, (strcmp(m_HabChangeTimes.c_str(),"0")==0));
	testFileExist_changeFile("--HAB_CHANGE_MASK--",m_HabChangeMasks, (strcmp(m_HabChangeTimes.c_str(),"0")==0));
}

void FOPL::checkCorrectParams_disp()
{
	testDirExist("--SAVE_DIR--",m_SavingDir+"/DISPERSAL/");
	testFileExist("--PFG_DISPERSAL_PARAMS--",m_FGDispersal, false);
}

void FOPL::checkCorrectParams_dist()
{
	testFileExist("--PFG_DISTURBANCES_PARAMS--",m_FGDisturbance, false);
	testFileExist("--DIST_MASK--",m_DisturbMask, false);
	testFileExist("--DIST_CHANGE_TIME--",m_DistChangeTimes, (strcmp(m_DistChangeTimes.c_str(),"0")==0));
	testFileExist("--DIST_CHANGE_MASK--",m_DistChangeMasks, (strcmp(m_DistChangeTimes.c_str(),"0")==0));
	testFileExist_changeFile("--DIST_CHANGE_MASK--",m_DistChangeMasks, (strcmp(m_DistChangeTimes.c_str(),"0")==0));
}

void FOPL::checkCorrectParams_soil()
{
	testDirExist("--SAVE_DIR--",m_SavingDir+"/SOIL/");
	testFileExist("--PFG_SOIL_PARAMS--",m_FGSoil, false);
}

void FOPL::checkCorrectParams_fire()
{
	testFileExist("--PFG_FIRES_PARAMS--",m_FGFire, false);
	testFileExist("--FIRE_MASK--",m_FireMask, false);
	testFileExist("--FIRE_CHANGE_TIME--",m_FireChangeTimes, (strcmp(m_FireChangeTimes.c_str(),"0")==0));
	testFileExist("--FIRE_CHANGE_MASK--",m_FireChangeMasks, (strcmp(m_FireChangeTimes.c_str(),"0")==0));
	testFileExist_changeFile("--FIRE_CHANGE_MASK--",m_FireChangeMasks, (strcmp(m_FireChangeTimes.c_str(),"0")==0));
	testFileExist("--FIRE_CHANGE_FREQUENCIES--",m_FireChangeFrequencies, true);
	testFileExist("--FIRE_FREQ_CHANGE_TIME--",m_FireFreqChangeTimes, true);
	testFileExist("--CLIM_DATA_MASK--",m_ClimDataMask, true);
	testFileExist("--CLIM_DATA_CHANGE_TIME--",m_ClimDataChangeTimes, (strcmp(m_ClimDataChangeTimes.c_str(),"0")==0));
	testFileExist("--CLIM_DATA_CHANGE_MASK--",m_ClimDataChangeMasks, (strcmp(m_ClimDataChangeTimes.c_str(),"0")==0));
	testFileExist_changeFile("--CLIM_DATA_CHANGE_MASK--",m_ClimDataChangeMasks, (strcmp(m_ClimDataChangeTimes.c_str(),"0")==0));
}

void FOPL::checkCorrectParams_drought()
{
	testFileExist("--PFG_DROUGHT_PARAMS--",m_FGDrought, false);
	testFileExist("--MOIST_MASK--",m_MoistIndexMask, false);
	testFileExist("--MOIST_CHANGE_TIME--",m_MoistIndexChangeTimes, (strcmp(m_MoistIndexChangeTimes.c_str(),"0")==0));
	testFileExist("--MOIST_CHANGE_MASK--",m_MoistIndexChangeMasks, (strcmp(m_MoistIndexChangeTimes.c_str(),"0")==0));
	testFileExist_changeFile("--MOIST_CHANGE_MASK--",m_MoistIndexChangeMasks, (strcmp(m_MoistIndexChangeTimes.c_str(),"0")==0));
}

void FOPL::checkCorrectParams_habStab()
{
	testFileExist("--HAB_MASK--",m_HabitatMask, false);
	testFileExist("--HAB_BASELINE_STATS--",m_HabitatBLstats, false);
}

void FOPL::checkCorrectParams_aliens()
{
	testFileExist("--ALIENS_MASK--",m_FGCondInitMaps, false);
	testFileExist("--ALIENS_CHANGE_TIME--",m_AliensChangeTimes, (strcmp(m_AliensChangeTimes.c_str(),"0")==0));
	testFileExist("--ALIENS_CHANGE_MASK--",m_AliensChangeMasks, (strcmp(m_AliensChangeTimes.c_str(),"0")==0));
	testFileExist_changeFile("--ALIENS_CHANGE_MASK--",m_AliensChangeMasks, (strcmp(m_AliensChangeTimes.c_str(),"0")==0));
	testFileExist("--ALIENS_CHANGE_FREQUENCIES--",m_AliensChangeFrequencies, true);
	testFileExist("--ALIENS_FREQ_CHANGE_TIME--",m_AliensFreqChangeTimes, true);
}

void FOPL::checkCorrectParams(const bool& doLight, const bool& doHabSuit, const bool& doDisp, const bool& doDist,
const bool& doSoil, const bool& doFire, const bool& doDrought, const bool& doHabStab, const bool& doAliens)
{
	cout << endl;
	cout << "===========> RUNNING Check of PARAM SIMUL FILES :" << endl;
	
	testFileExist("--GLOBAL_PARAMS--",m_GlobSimulParams, false);
	testFileExist("--SAVED_STATE--",m_SavedState, true);
	
	testDirExist("--SAVE_DIR--",m_SavingDir);
	testDirExist("--SAVE_DIR--",m_SavingDir+"/ABUND_perPFG_perStrata/");
	testDirExist("--SAVE_DIR--",m_SavingDir+"/ABUND_perPFG_allStrata/");
	testDirExist("--SAVE_DIR--",m_SavingDir+"/ABUND_allPFG_perStrata/");
	
	testFileExist("--ARRAYS_SAVING_YEARS--",m_SummArrSavingTimes, true);
	testFileExist("--OBJECTS_SAVING_YEARS--",m_ObjectsSavingTimes, true);
	
	testFileExist("--MASK--",m_Mask, false);
	testFileExist("--MASK_CHANGE_TIME--",m_MaskChangeTimes, true);
	testFileExist("--MASK_CHANGE_MASK--",m_MaskChangeMasks, true);
	testFileExist_changeFile("--MASK_CHANGE_MASK--",m_MaskChangeMasks, true);
	
	testFileExist("--PFG_LIFE_HISTORY_PARAMS--",m_FGLifeHistory, false);

	if (doLight) checkCorrectParams_light();
	if (doHabSuit) checkCorrectParams_habSuit();
	if (doDisp) checkCorrectParams_disp();
	if (doDist) checkCorrectParams_dist();
	if (doSoil) checkCorrectParams_soil();
	if (doFire) checkCorrectParams_fire();
	if (doDrought) checkCorrectParams_drought();
	if (doHabStab) checkCorrectParams_habStab();
	if (doAliens) checkCorrectParams_aliens();
	
	cout << "===========> Check OK!" << endl;
	cout << endl;
} // end of checkCorrectParams(...)

void FOPL::checkCorrectParams()
{
	cout << endl;
	cout << "===========> RUNNING Check of PARAM SIMUL FILES :" << endl;
	
	testFileExist("--GLOBAL_PARAMS--",m_GlobSimulParams, false);
	testFileExist("--SAVED_STATE--",m_SavedState, true);

	testDirExist("--SAVE_DIR--",m_SavingDir);
	testDirExist("--SAVE_DIR--",m_SavingDir+"/ABUND_perPFG_perStrata/");
	testDirExist("--SAVE_DIR--",m_SavingDir+"/ABUND_perPFG_allStrata/");
	testDirExist("--SAVE_DIR--",m_SavingDir+"/ABUND_allPFG_perStrata/");
	testDirExist("--SAVE_DIR--",m_SavingDir+"/SOIL/");
	testDirExist("--SAVE_DIR--",m_SavingDir+"/LIGHT/");
	testDirExist("--SAVE_DIR--",m_SavingDir+"/DISPERSAL/");

	testFileExist("--ARRAYS_SAVING_YEARS--",m_SummArrSavingTimes, true);
	testFileExist("--OBJECTS_SAVING_YEARS--",m_ObjectsSavingTimes, true);
	testFileExist("--MASK--",m_Mask, false);

	testFileExist("--HAB_MASK--",m_HabitatMask, true);
	testFileExist("--DIST_MASK--",m_DisturbMask, true);
	testFileExist("--FIRE_MASK--",m_FireMask, true);
	testFileExist("--MOIST_MASK--",m_MoistIndexMask, true);
	testFileExist("--SLOPE_MASK--",m_SlopeMask, true);
	testFileExist("--CLIM_DATA_MASK--",m_ClimDataMask, true);
	testFileExist("--MASK_CHANGE_TIME--",m_MaskChangeTimes, true);
	testFileExist("--MASK_CHANGE_MASK--",m_MaskChangeMasks, true);
	testFileExist_changeFile("--MASK_CHANGE_MASK--",m_MaskChangeMasks, true);
	testFileExist("--HAB_CHANGE_TIME--",m_HabChangeTimes, true);
	testFileExist("--HAB_CHANGE_MASK--",m_HabChangeMasks, true);
	testFileExist_changeFile("--HAB_CHANGE_MASK--",m_HabChangeMasks, true);
	testFileExist("--DIST_CHANGE_TIME--",m_DistChangeTimes, true);
	testFileExist("--DIST_CHANGE_MASK--",m_DistChangeMasks, true);
	testFileExist_changeFile("--DIST_CHANGE_MASK--",m_DistChangeMasks, true);
	testFileExist("--FIRE_CHANGE_TIME--",m_FireChangeTimes, true);
	testFileExist("--FIRE_CHANGE_MASK--",m_FireChangeMasks, true);
	testFileExist_changeFile("--FIRE_CHANGE_MASK--",m_FireChangeMasks, true);
	testFileExist("--FIRE_CHANGE_FREQUENCIES--",m_FireChangeFrequencies, true);
	testFileExist("--FIRE_FREQ_CHANGE_TIME--",m_FireFreqChangeTimes, true);
	testFileExist("--CLIM_DATA_CHANGE_TIME--",m_ClimDataChangeTimes, true);
	testFileExist("--CLIM_DATA_CHANGE_MASK--",m_ClimDataChangeMasks, true);
	testFileExist_changeFile("--CLIM_DATA_CHANGE_MASK--",m_ClimDataChangeMasks, true);
	testFileExist("--MOIST_CHANGE_TIME--",m_MoistIndexChangeTimes, true);
	testFileExist("--MOIST_CHANGE_MASK--",m_MoistIndexChangeMasks, true);
	testFileExist_changeFile("--MOIST_CHANGE_MASK--",m_MoistIndexChangeMasks, true);

	testFileExist("--PFG_LIFE_HISTORY_PARAMS--",m_FGLifeHistory, false);
	testFileExist("--PFG_LIGHT_PARAMS--",m_FGLight, true);
	testFileExist("--PFG_HAB_MASK--",m_FGHabSuitMaps, true);
	testFileExist("--PFG_DISPERSAL_PARAMS--",m_FGDispersal, true);
	testFileExist("--PFG_DISTURBANCES_PARAMS--",m_FGDisturbance, true);
	testFileExist("--PFG_SOIL_PARAMS--",m_FGSoil, true);
	testFileExist("--PFG_FIRES_PARAMS--",m_FGFire, true);
	testFileExist("--PFG_DROUGHT_PARAMS--",m_FGDrought, true);
	testFileExist("--ALIENS_MASK--",m_FGCondInitMaps, true);

	testFileExist("--HAB_BASELINE_STATS--",m_HabitatBLstats, true);
	testFileExist("--ALIENS_CHANGE_TIME--",m_AliensChangeTimes, true);
	testFileExist("--ALIENS_CHANGE_MASK--",m_AliensChangeMasks, true);
	testFileExist_changeFile("--ALIENS_CHANGE_MASK--",m_AliensChangeMasks, true);
	testFileExist("--ALIENS_CHANGE_FREQUENCIES--",m_AliensChangeFrequencies, true);
	testFileExist("--ALIENS_FREQ_CHANGE_TIME--",m_AliensFreqChangeTimes, true);
	
	cout << "===========> Check OK!" << endl;
	cout << endl;
} // end of checkCorrectParams(...)

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void FOPL::testSameCoord(const string& param, const string& file_name, const string& ext_REF, Coordinates<double>& coord_REF)
{
	if (file_name != "0")
	{
		boost::filesystem::path file_to_test(file_name);
		if (file_to_test.extension()!=ext_REF)
		{ // TEST file extension
			cerr << "!!! Parameter " << param << " : the file " << file_name << " does not have the same extension than --MASK-- file. Please check!" << endl;
			terminate();
		}
		Coordinates<double> coord_to_test = ReadCoordinates(file_name);
		if (!(coord_to_test==coord_REF))
		{ // TEST file coordinates
			cout << "> TESTED Coordinates - Xmax : " << coord_to_test.getXmax() << endl;
			cout << "> TESTED Coordinates - Xmin : " << coord_to_test.getXmin() << endl;
			cout << "> TESTED Coordinates - Xres : " << coord_to_test.getXres() << endl;
			cout << "> TESTED Coordinates - Xncell : " << coord_to_test.getXncell() << endl;
			cout << "> TESTED Coordinates - Ymax : " << coord_to_test.getYmax() << endl;
			cout << "> TESTED Coordinates - Ymin : " << coord_to_test.getYmin() << endl;
			cout << "> TESTED Coordinates - Yres : " << coord_to_test.getYres() << endl;
			cout << "> TESTED Coordinates - Yncell : " << coord_to_test.getYncell() << endl;
			cout << "> TESTED Coordinates - Totncell : " << coord_to_test.getTotncell() << endl;
			cerr << "!!! Parameter " << param << " : the file " << file_name << " does not have the same coordinates than --MASK-- file. Please check!" << endl;
			terminate();
		}
	}
}

void FOPL::testSameCoord(const string& param, vector<string> vector_name, const string& ext_REF, Coordinates<double>& coord_REF)
{
	for (vector<string>::iterator file_name=vector_name.begin(); file_name!=vector_name.end(); ++file_name)
	{
		testSameCoord(param, *file_name, ext_REF, coord_REF);
	}
}

void FOPL::checkCorrectMasks()
{
	cout << endl;
	cout << "===========> RUNNING Check of RASTER MASKS :" << endl;
	
	/* Get mask extension */
	boost::filesystem::path mask_path(m_Mask.c_str());
	string ext_REF = mask_path.extension().string();
	
	if (ext_REF==".asc")
	{ // ASCII file
		cerr << "!!! The --MASK-- file is an ASCII file. You must provide either a .img or .tif file." << endl;
		terminate();
	}
	if (ext_REF!=".img" && ext_REF!=".tif")
	{ // ASCII file
		cerr << "!!! The extension of the --MASK-- file (" << ext_REF << ") is not supported. You must provide either a .img or .tif file." << endl;
		terminate();
	}
	
	/* Get mask coordinates */
	Coordinates<double> coord_REF = ReadCoordinates(m_Mask);
	cout << ">> MASK Coordinates - Xmax : " << coord_REF.getXmax() << endl;
	cout << ">> MASK Coordinates - Xmin : " << coord_REF.getXmin() << endl;
	cout << ">> MASK Coordinates - Xres : " << coord_REF.getXres() << endl;
	cout << ">> MASK Coordinates - Xncell : " << coord_REF.getXncell() << endl;
	cout << ">> MASK Coordinates - Ymax : " << coord_REF.getYmax() << endl;
	cout << ">> MASK Coordinates - Ymin : " << coord_REF.getYmin() << endl;
	cout << ">> MASK Coordinates - Yres : " << coord_REF.getYres() << endl;
	cout << ">> MASK Coordinates - Yncell : " << coord_REF.getYncell() << endl;
	cout << ">> MASK Coordinates - Totncell : " << coord_REF.getTotncell() << endl;
	
	testSameCoord("--PFG_HAB_MASK--",m_FGHabSuitMaps,ext_REF,coord_REF);
	testSameCoord("--DIST_MASK--",m_DisturbMask,ext_REF,coord_REF);
	
	testSameCoord("--FIRE_MASK--",m_FireMask,ext_REF,coord_REF);
	testSameCoord("--CLIM_DATA_MASK--",m_ClimDataMask,ext_REF,coord_REF);
	testSameCoord("--MOIST_MASK--",m_MoistIndexMask,ext_REF,coord_REF);
	
	testSameCoord("--ALIENS_MASK--",m_FGCondInitMaps,ext_REF,coord_REF);
	
	testSameCoord("--HAB_MASK--",m_HabitatMask,ext_REF,coord_REF);
	
	cout << "===========> Check OK!" << endl;
	cout << endl;
} // end of checkCorrectMasks(...)



