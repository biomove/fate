CC            = gcc
CXX           = g++
DEFINES       = -DUNICODE -DQT_LARGEFILE_SUPPORT -DQT_DLL -DQT_NO_DEBUG -DQT_CORE_LIB -DQT_THREAD_SUPPORT
CFLAGS        = -O2 -Wall $(DEFINES)
CXXFLAGS      = -O2 -frtti -fexceptions -Wall -fopenmp -std=c++11 -w $(DEFINES)

$(info Version CXX used is $(CXX))
$(info )

GDAL_CFLAGS = $(shell gdal-config --cflags)
GDAL_LDFLAGS = $(shell gdal-config --libs)

#######################################################################################
### Define directories path
#######################################################################################

CURRENT_DIR= $(shell pwd)
MAKEFILE_PATH= $(abspath $(lastword $(MAKEFILE_LIST)))
MAKEFILE_DIR= $(patsubst %/,%,$(dir $(MAKEFILE_PATH)))
DEVEL_DIR= $(patsubst %/,%,$(dir $(MAKEFILE_DIR)))
SOURCES_DIR= 
PREFIX = /usr/local
SUPPMAT_DIR= $(PREFIX)/lib/fate/SUPP_MAT/

$(info CURRENT_DIR is $(CURRENT_DIR))
$(info MAKEFILE_PATH is $(MAKEFILE_PATH))
$(info MAKEFILE_DIR is $(MAKEFILE_DIR))
$(info SOURCES_DIR is $(SOURCES_DIR))
$(info SUPPMAT_DIR is $(SUPPMAT_DIR))
$(info )

#######################################################################################
### Define file name
#######################################################################################

VERSION= 6.2-0
EXEC_DIR= $(MAKEFILE_DIR)/bin
EXEC= $(EXEC_DIR)/FATEHDD_$(VERSION)

$(info File that will be created is $(EXEC))

#######################################################################################
### Define INCLUDE directories
#######################################################################################

GDAL_INCLUDE= $(GDAL_CFLAGS)
BOOST_INCLUDE=

INCLUDEDIRS= $(GDAL_INCLUDE) $(BOOST_INCLUDE)


$(info INCLUDE_DIR : $(INCLUDEDIRS))
$(info )

#######################################################################################
### Define LIBRARY directories
#######################################################################################

GDAL_LIB= $(GDAL_LDFLAGS)
GDAL_LIB= -lgdal
BOOST_LIB= -lboost_serialization -lboost_filesystem -lboost_system
GCC_LIB= -lgomp

LDFLAGS= $(GDAL_LIB) $(BOOST_LIB) $(GCC_LIB)


$(shell export LD_LIBRARY_PATH=/usr/lib/)

$(info LIB_DIR : $(LDFLAGS))
$(info )


#######################################################################################
### MAKE FILES
#######################################################################################


all: $(EXEC)

$(EXEC):  main_CROSSPLATFORM.o Cohort.o Community.o Disp.o FG.o FGresponse.o FGUtils.o FilesOfParamsList.o FuncGroup.o GlobalSimulParameters.o Legion.o LightResources.o  Params.o PropPool.o SimulMap.o SuFate.o SuFateH.o
	mkdir -p $(EXEC_DIR)
	$(CXX) -o $@ $^ $(LDFLAGS) $(INCLUDEDIRS)

main_CROSSPLATFORM.o: $(SOURCES_DIR)main_CROSSPLATFORM.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

Cohort.o: $(SOURCES_DIR)Cohort.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS) 

Community.o: $(SOURCES_DIR)Community.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

Disp.o: $(SOURCES_DIR)Disp.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FG.o: $(SOURCES_DIR)FG.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FGresponse.o: $(SOURCES_DIR)FGresponse.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FGUtils.o: $(SOURCES_DIR)FGUtils.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FilesOfParamsList.o: $(SOURCES_DIR)FilesOfParamsList.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FuncGroup.o: $(SOURCES_DIR)FuncGroup.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

GlobalSimulParameters.o: $(SOURCES_DIR)GlobalSimulParameters.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

Legion.o: $(SOURCES_DIR)Legion.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

LightResources.o: $(SOURCES_DIR)LightResources.cpp  
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

Params.o: $(SOURCES_DIR)Params.cpp  
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS) 

PropPool.o: $(SOURCES_DIR)PropPool.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

SimulMap.o: $(SOURCES_DIR)SimulMap.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

SuFate.o: $(SOURCES_DIR)SuFate.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

SuFateH.o: $(SOURCES_DIR)SuFateH.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

%:

.PHONY: clean mrproper

install: $(EXEC)
	mkdir -p $(DESTDIR)/$(PREFIX)/bin
	install bin/* $(DESTDIR)/$(PREFIX)/bin

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)
